var loader = document.getElementById("load");
      var conter = document.getElementById("count");
      var i = 0;
      var L = setInterval(function () {
        
        i++;
        if (i > 100) {clearInterval(L)}
      }, 20);
      var dropzone = document.getElementById("dropzone");

      var Ajax = function (data) {
        $.ajax({
          url: '',
          type: 'POST',
          data: data,
          //For FormData
          processData: false,
          contentType: false,
          //Progress
          xhr: function(){
           var xhr = new window.XMLHttpRequest();
             //Upload progress
           xhr.upload.addEventListener("progress", function(evt){
               if (evt.lengthComputable) {
                  var percentComplete = evt.loaded / evt.total;
                  //Do something with upload progress
                  let _p = Math.round(percentComplete*100);
                  loader.style.width = _pgitgit+"%";
                  conter.innerHTML = _pgitgit+"%";
               }
           }, false);
           //Download progress
           xhr.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                  var percentComplete = evt.loaded / evt.total;
                  //Do something with download progress
                  console.log(percentComplete);
                }
           }, false);

           return xhr;
         }
        })
        .done(function() {
          console.log("success");
        })
        .fail(function() {
          console.log("Error");
        })
      }
      
      

      var upload = function (files) {
        var data = new FormData();
        for (var i = 0; i < files.length; i++) {
          data.append('userfile[]',files[i]);
        }
        data.append('snimi',true);
        return data;
      }

      dropzone.ondrop = function (e) {
        e.preventDefault();
        var userfile = upload(e.dataTransfer.files);
        console.log(userfile)
        Ajax(userfile);
        //console.log(e.dataTransfer.files);
      }

      dropzone.ondragover = function (e) {
        e.preventDefault();
        this.className = "dragover";
      }

      dropzone.ondragleave = function () {
        this.className = "";
      }