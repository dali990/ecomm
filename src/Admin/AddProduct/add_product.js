import Vue from 'vue'
import AddProduct from './AddProduct.vue'

new Vue({
  el: '#AddProduct',
  render: h => h(AddProduct)
})