-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2018 at 02:54 PM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `silluzio`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(100) DEFAULT NULL,
  `parent_id` smallint(16) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `parent_id`) VALUES
(1, 'Man', 0),
(2, 'Woman', 0),
(3, 'Watches', 1),
(4, 'Parfumes', 1),
(5, 'Jackets', 1),
(6, 'Blazers', 1),
(7, 'T-shirts', 1),
(8, 'Shirts', 1),
(9, 'Pants', 1),
(10, 'Shorts', 1),
(11, 'Shoes', 1),
(12, 'Dreses', 2),
(13, 'Blouses', 2),
(14, 'Skirts', 2),
(15, 'Tanks', 1),
(16, 'Polo', 1),
(17, 'Watches', 2),
(18, 'Parfumes', 2),
(19, 'Jackets', 2),
(20, 'Blazers', 2),
(21, 'T-shirts', 2),
(22, 'Shirts', 2),
(23, 'Pants', 2),
(24, 'Shorts', 2),
(25, 'Shoes', 2),
(26, 'Accessories', 0),
(43, 'NOVO', 26),
(46, 'H&M', 26),
(50, 'NB', 0),
(51, 'dsd3', 50),
(52, 'NB sbu 1', 50),
(53, 'A', 0);

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(11) NOT NULL,
  `color_name` varchar(100) DEFAULT NULL,
  `quantity` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `color_name`, `quantity`) VALUES
(1, 'Plava', 0),
(2, 'Bela', 0),
(3, 'Crna', 0),
(4, 'Crvena', 0),
(5, 'Siva', 0),
(14, 'Zelena', 0),
(20, 'dd', NULL),
(21, 'JOS', NULL),
(22, 'a', NULL),
(25, 'dadsad sd asdasdsaa sad', NULL),
(26, 'DA', NULL),
(27, 'DA', NULL),
(28, 'sss', NULL),
(29, 'ES', NULL),
(30, 'D4', NULL),
(31, 'd5', NULL),
(32, 'd6', NULL),
(33, 'd7', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `price` decimal(11,0) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `discount_price` decimal(11,0) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `sub_category` varchar(16) DEFAULT NULL,
  `group_category` varchar(16) DEFAULT NULL,
  `description` varchar(756) DEFAULT NULL,
  `info` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `quantity`, `discount_price`, `category`, `sub_category`, `group_category`, `description`, `info`) VALUES
(1, 'Roba 1', '2500', 18, '1900', 1, NULL, NULL, '', NULL),
(2, 'Roba 2', '2600', 12, '2200', 2, NULL, NULL, '', NULL),
(3, 'Roba 3', '3000', 5, '2500', 3, NULL, NULL, '', NULL),
(4, 'Roba 4', '2600', 10, NULL, 4, NULL, NULL, '', NULL),
(5, 'Pantalone', '3500', 10, '0', 4, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE `product_color` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `quantity_color` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_color`
--

INSERT INTO `product_color` (`id`, `product_id`, `color_id`, `quantity_color`) VALUES
(1, 1, 2, 4),
(2, 1, 5, 6),
(3, 2, 4, 6),
(4, 2, 14, 4),
(5, 3, 1, 3),
(6, 4, 2, 5),
(10, 5, 1, 5),
(11, 5, 3, 2),
(12, 5, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE `product_size` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `quantity_size` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`id`, `product_id`, `size_id`, `quantity_size`) VALUES
(1, 1, 4, 3),
(2, 1, 5, 2),
(3, 1, 10, 5),
(4, 1, 11, 2),
(5, 2, 3, 9),
(6, 2, 5, 12),
(19, 5, 7, 3),
(20, 5, 8, 2),
(21, 5, 9, 4),
(22, 5, 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `shape`
--

CREATE TABLE `shape` (
  `id` int(11) NOT NULL,
  `name` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shape`
--

INSERT INTO `shape` (`id`, `name`) VALUES
(1, 'Slim'),
(2, 'Fit'),
(3, 'Large');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `size_name` varchar(100) NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `sub_category` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `size_name`, `category_id`, `sub_category`) VALUES
(2, 'XS', 2, 21),
(3, 'S', 2, 21),
(4, 'M', 2, 21),
(5, 'L', 2, 21),
(6, 'XL', 2, 21),
(7, '36', 2, 23),
(8, '38', 2, 23),
(9, '39', 2, 23),
(10, '40', 2, 23),
(11, '41', 2, 23),
(12, '110A', 1, 16),
(13, '120B', 1, 16),
(14, '130C', 1, 16),
(15, 'A', 26, 43),
(16, 'B', 26, 43),
(17, 'C', 26, 43),
(18, 'D', 26, 43),
(21, 'E', 26, 43),
(22, 'F', 26, 43);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shape`
--
ALTER TABLE `shape`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategorija_id` (`category_id`),
  ADD KEY `sub_category` (`sub_category`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `shape`
--
ALTER TABLE `shape`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `sizes`
--
ALTER TABLE `sizes`
  ADD CONSTRAINT `sizes_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `sizes_ibfk_2` FOREIGN KEY (`sub_category`) REFERENCES `categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
