<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

interface RequestInterface{
	function total();
	function to_array();
}
/**
* 
*/
class Request implements RequestInterface
{
	private static $R = null;
	private  function __construct()
	{
		# code...
	}

	private function __clone(){

	}

	public static function init()
	{
		if (self::$R == null) {
			self::$R = new Request();
			return self::$R;
		}
	}

	protected $data = [];

	public function __set($name, $value)
  {
    $this->data[$name] = $value;
  }

  public function __get($name)
  {
  	if (array_key_exists($name, $this->data)) {
        return $this->data[$name];
    }
  }

  public function total()
  {
  	return count($this->data);
  }

  public function to_array($value='')
  {
  	return $this->data;
  }

  public function exists($key)
  {
  	return array_key_exists($key, $this->data);
  }
}


 ?>