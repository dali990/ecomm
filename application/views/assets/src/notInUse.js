// add_product.js
// Prvi progress handler tokom upload fajlova
  var Ajax = function (data) {
    $.ajax({
      url: '',
      type: 'POST',
      data: data,
      //For FormData
      processData: false,
      contentType: false,
      //Progress
      xhr: function(){
        var xhr = new window.XMLHttpRequest();
        //Upload progress
        xhr.upload.addEventListener("progress", function(evt){
          if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            //Do something with upload progress
            let _p = Math.round(percentComplete*100);
            loader.style.width = _p+"%";
            counter.innerHTML = _p+"%";
          }
        }, false);
        //Download progress
        xhr.addEventListener("progress", function(evt){
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              //Do something with download progress
              console.log(percentComplete);
            }
        }, false);

        return xhr;
      }
    })
    .done(function() {
      console.log("upload success");
    })
    .fail(function() {
      console.log("Error while files uploading.");
    })
  }

  // Prikaz i skrivanje inputa za kolicinu velicine izabrane kategorije
  // NO AJAX
  // Handles the quantity size form view of selected categroy
  function hideCategorySizes(){   
    let options = select_category.options;
    for (var i = 1; i < options.length; i++) {
      // Selecting all colums with inputs for selected category
      let size_inputs = document.getElementsByClassName("category_"+options[i].value);
      for (var j = 0; j < size_inputs.length; j++) {     
        if (size_inputs[j] != null) {
          size_inputs[j].classList.add("hidden");
        }
      }
    }
  }

  hideCategorySizes(); // Bolje resenje, dodati klasu hidden svakoj div koloni

  select_category.onchange = function (el) {
    hideCategorySizes();
    let size_inputs = document.getElementsByClassName("category_"+this.value);
    for (var j = 0; j < size_inputs.length; j++) {     
      if (size_inputs[j] != null) {
        size_inputs[j].classList.remove("hidden");
      }
    }
  };