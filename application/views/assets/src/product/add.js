// Product/add/
require('../main.js');
require('./render_helpers');
window.AJAX = require('../components/ajax');
import SubRender from '../components/subRender';
import Validate from '../components/validate';

  // HTML select element 
  window.input_sizes = document.getElementById("category_parent");
  let select_category = document.getElementById("select_category");
  let select_sub_category = document.getElementById("select_sub_category");

  select_category.onchange = function (el) {
    //Removes all inputs for size and quantity on select_category change
    input_sizes.innerHTML = "";
    SubRender.getSubCategories(this.value,select_sub_category);
  };

  // Inserts the appropriate sizes
  select_sub_category.onchange = function (el) {
    //getCategorySizes(select_category.value, this.value)
    SubRender.fill({
      'container':'category_parent',
      'db_field':'size_name',
      'get':'getSizes',
      id : select_category.value,
      sub_id : this.value,
      //'delte_row':false,
      callback: function (container,obj) {
        container.innerHTML += `
        <div class="col-md-2 col-sm-2 col-xs-4 ">
            ${obj.size_name}
            <input class="form-control size" type="number" min="0" name="quantity_size[${obj.id}]"
             style="width: 70px;" >
          </div>
        `;
      }
    })
  };

  // Loader for file upload 
  window.userfiles = null;
  let loader = document.getElementById("load");
  let counter = document.getElementById("count");
  let dropzone = document.getElementById("dropzone");
  let dropzone_msg = document.getElementById("dropzone_msg");

  dropzone.ondrop = function (e) {
    e.preventDefault();
    //console.log(e.dataTransfer.files);
    //Removes occured errors
    counter.innerHTML = '0%';
    dropzone_msg.innerHTML = '';
    userfiles = upload(e.dataTransfer.files);
    if (userfiles != false) {
      //startUpload(userfiles);
      V.execute(startUpload,userfiles);
    }
  }

  dropzone.ondragover = function (e) {
    e.preventDefault();
    this.className = "dragover";
  }

  dropzone.ondragleave = function () {
    this.className = "";
  }

  // Validates and formats dropet files in form data
  var upload = function (files) {
    var data = new FormData();
    for (var i = 0; i < files.length; i++) {
      if (files[i].type.split('/')[0] == 'image') {
        data.append('userfile[]',files[i]);
      } else {
        console.log('Not image');
        dropzone_msg.classList.add('alert-danger');
        dropzone_msg.classList.remove('alert-success');
        dropzone_msg.innerHTML=`File '${files[i].name}' is not an image!`;
        return false;
      }
    }
    data.append('snimi',true); //appends data for submiting
    data.append('csrf_token',csrf_token());
    return data;
  }

  window.startUpload = function (files) {
    console.log("Uploading...");
    return new Promise(function(resolve, reject){ 
      AJAX.progress({
        url: 'do_upload',
        data: files
      }, function(p){
        counter.innerHTML = p+"%";
        loader.style.width = p+"%";
        if(p==100){
          dropzone_msg.classList.add("alert-success");
          dropzone_msg.classList.remove("alert-danger");
          dropzone_msg.innerHTML=`File(s) upload complete.`;
          resolve(true);
        }
      });
    });
    /*.then(function (p) {
      counter.innerHTML = p+"%";
      loader.style.width = p+"%";
      if(p==100){
        dropzone_msg.classList.add("alert-success");
        dropzone_msg.classList.remove("alert-danger");
        dropzone_msg.innerHTML=`File(s) upload complete.`;
      }
    })
    .catch(function (msg) {
      console.log(msg);
    })*/
  }      

  // Validation
  window.V = new Validate('add_product');
  V.setRules({
    name: {name:"Name", rule:"required|string"},
    price: {name:"Price", rule:"required|number"},
    quantity: {name:"Quantity", rule:"number"},
    category: {name:"Category", rule:"required|string"},
    discount_price: {name:"Discount Price", rule:"number"},
    sub_category: {name:"Sub Category", rule:"required|string"},
    quantity_size: {name:"Quantity size", rule:"array_check:number", array:"size"},
    quantity_color: {name:"Quantity color", rule:"array_check:number", array:"color"},
  });
  
  V.run();


