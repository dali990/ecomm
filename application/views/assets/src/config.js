//Za require()
//window.BASE_URL = 'http://localhost/ongit/project/ci/';

//Za const APP = require()
//export let BASE_URL = 'http://localhost/ongit/project/ci/';

//Za import * as APP ili import {BASE_URL}...
/**
 * [BASE_URL description]
 * @type {String}
 */
export const BASE_URL = 'http://localhost/ongit/project/ci/';

/**
 * App configuration
 * @type {Object}
 */
const APP = {
	base_url : 'http://localhost/ongit/project/ci/',
	lng : 'EN'
};
Object.defineProperty(APP, 'api_url', {
	get: function () {
		return this.base_url+"ajax/addproduct/";
	}
});
export {APP};

/**
 * Validation language extension 
 * @type {Object}
 */
export let V_LNG = {
	SR : {
		required: {
			msg:"Polje {field} nesme biti prazno.",
			r(field_name){
				return this.msg.replace("{field}",field_name);
			}
		},
		number  : "Polje moze da sadrzi samo brojeve.",
		max     : {
			msg: "Polje {field} može da sadrži najviše {param} karaktera.",
			r(field_name, param){
				return this.msg.replace("{field}",field_name).replace("{param}",param);
			}
		}
	},
	EN : {
		required: "Field can not be empty.",
		number  : "Field can contain only numbers."
	}
}

//-------------------------------------
//Ekvivalent module.exports
/*let BASE_URL = 'http://localhost/ongit/project/ci/';
export default BASE_URL;*/

//Za import APP from ...
/*module.exports = {
	BASE_URL : 'http://localhost/ongit/project/ci/',
	LNG : 'EN'
}*/

