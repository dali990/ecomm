// productSettings/sizes
import Vue from 'vue';
require('../main.js');
require('./render_helpers');
window.AJAX = require('../components/ajax');
import SubRender from '../components/subRender';
import Validate from '../components/validate';
import vuec from './vuec';

let select_category = document.getElementById("select_category");
let select_sub_category = document.getElementById("select_sub_category");

// Validation
window.V = new Validate('add_sizes');
  V.setRules({
    category_id: {name:"Select category", rule:"required|number"},
    sub_category: {name:"Sub Category", rule:"required|number"},
    size: {name:"Size", rule:"max:10|array_check:string", array:true}
  });
V.run();

// Subcategory render
select_category.onchange = function (el) {
  SubRender.getSubCategories(this.value,select_sub_category);
};

let params = {
	'container':'table_sizes',
	'db_field':'size_name',
	'get':'getSizes',
	counter: 1,
	//'delte_row':false,
	callback: function (container,obj) {
		container.innerHTML += `
		<tr>
      <td>${this.counter++}</td>
      <td>${obj.size_name}</td>
    	<td>
    		<form action="sizes/delete" method="post" onsubmit="return confirm('Izbrisati ?');">
    			<input type="hidden" name="csrf_token" value="${csrf_token()}">
    			<input type="hidden" name="_method" value="DELETE">
      		<input type="hidden" name="size_id" value="${obj.id}">
      		<input type="hidden" name="size_name" value="${obj.size_name}">
      		<button type="submit" class="btn btn-sm">
      			<span class="glyphicon glyphicon-trash text-danger"></span>
      		</button>
     		</form>
      </td>         
    </tr>
		`;
	}
};

// Reads sizes
select_sub_category.onchange = function (el) {
  //getCategorySizesTable(select_category.value,this.value);
  params.id = select_category.value;
  params.sub_id = this.value;
  SubRender.fill(params)
};

// Inserts inputs for sizes
let div = document.getElementById('insert_more_sizes');
window.addSizes = function() {
	let size = `
		<div class="form-group col-md-2 col-sm-4 col-xs-4">
      <input type="text" class="form-control" name="size[]" style="padding: 3px">
    </div>
	`;
	let div_form_group = document.createElement('div');
	div_form_group.classList = "form-group col-md-2 col-sm-4 col-xs-4";
	let input = document.createElement("input");
	input.classList = "form-control";
	input.style.padding = '3px';
	input.name = "size[]"; 
	div_form_group.appendChild(input);
	div.appendChild(div_form_group);
}

/*
new Vue({
  el: '#sizes_vue',
  //el: '#vue_c',
  components:{
    'vuec':vuec
  },
  data(){
    return {
      //div: document.getElementById('insert_more_sizes'),
      div: this.$refs.insert_more_sizes,
      sizes: `
          <div class="form-group col-md-2 col-sm-4 col-xs-4">
            <input type="text" class="form-control" name="size[]" style="padding: 3px">
          </div>
      `,
      range: 3
    }
  },
  methods:{
    addSizes: function(){
      //console.log(this.$refs);
      //this.sizes+=this.sizes;
      //this.div.innerHTML += this.size;
      console.log("RADI");
      this.range += 1;
    }
  },
  mounted: function() {
      this.div = this.$refs.insert_more_sizes;
  }
})
*/

