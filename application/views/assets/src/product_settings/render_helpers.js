
window.getSubCategoriesTable = function (id) {
	let table_sub_categories = document.getElementById("table_sub_categories");
	let counter = 1;

	AJAX.post({
		url: 'http://localhost/ongit/project/ci/ajax/addproduct/getSubCategories/'+id,
		//csrf: "csrf_test_name",
		//responseType: 'text'
	})
	.then(function (subs) {
		// Clears the table
		table_sub_categories.innerHTML = "";
		subs.forEach(function (sub) {
			table_sub_categories.innerHTML += `
				<tr>
          <td>${counter++}</td>
          <td>${sub.category_name}</td>
        	<td>
        		<form action="categories/delete_subcategory" method="post" onsubmit="return confirm('Izbrisati ?');">
        			<input type="hidden" name="csrf_token" value="${csrf_token()}">
        			<input type="hidden" name="_method" value="DELETE">
          		<input type="hidden" name="category_id" value="${sub.id}">
          		<input type="hidden" name="category_name" value="${sub.category_name}">
          		<button type="submit" class="btn btn-sm">
          			<span class="glyphicon gglyphicon glyphicon-trash text-danger"></span>
          		</button>
         		</form>
          </td>         
        </tr>
			`;
		})
	})
}

// Puts all sizes for specific category in the table
window.getCategorySizesTable = function (id, sub_id) {
	let table_sizes = document.getElementById("table_sizes");
	let counter = 1;
	AJAX.post({
		url: 'http://localhost/ongit/project/ci/ajax/addproduct/getSizes/'+id+'/'+sub_id,
		//responseType: 'text'
	})
	.then(function (sizes) {
		table_sizes.innerHTML = "";
		if (sizes.length == 0) {
			table_sizes.innerHTML = `<tr><td colspan="3"><label>Empty.</label></td></tr>`;
		}
		sizes.forEach(function (size) {
			table_sizes.innerHTML += `
				<tr>
          <td>${counter++}</td>
          <td>${size.size_name}</td>
        	<td>
        		<form action="" method="post" onsubmit="return confirm('Izbrisati ?');">
        			<input type="hidden" name="csrf_token" value="${csrf_token()}">
          		<input type="hidden" name="delete_size" value="${size.id}">
          		<input type="hidden" name="size_name" value="${size.size_name}">
          		<button type="submit" class="btn btn-sm">
          			<span class="glyphicon gglyphicon glyphicon-trash text-danger"></span>
          		</button>
         		</form>
          </td>         
        </tr>
			`;
		})
	})
}


