require('../main.js');
window.AJAX = require('../components/ajax');
import SubRender from '../components/subRender';

let params = {
	'container':'table_colors',
	'db_field':'color_name',
	'get':'getColors',
	//'delte_row':false,
	callback: function (container,obj) {
				container.innerHTML += `
				<div class="crud-item">
          <span class="crud-title">${obj[this.db_field]}</span>
          <form class="crud-action">
            <button type="submit" name="edit" class=" btn crud-edit" style="border: 0">
              <span class="glyphicon glyphicon-pencil "></span>
            </button>
          </form>          
          <form action="colors/delete" method="post" onsubmit="return confirm('Izbrisati ?');" class="crud-action">
          	<input type="hidden" name="csrf_token" value="${csrf_token()}">
          	<input type="hidden" name="_method" value="DELETE">
          	<input type="hidden" name="color_id" value="${obj.id}">
          	<input type="hidden" name="${this.db_field}" value="${obj[this.db_field]}">
            <button type="submit" name="delete" class=" btn crud-delete" style="border: 0">
              <span class="glyphicon glyphicon-trash text-danger "></span>
            </button>        
          </form>       
        </div>
				`;
			}
};
SubRender.fill(params);