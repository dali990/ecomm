require('../main.js');
require('./render_helpers');
window.AJAX = require('../components/ajax');
import validate from '../components/validate';
import SubRender from '../components/subRender';

window.V = validate;
//Simple form validation
var valid = new V('validation');
valid.setRules({
	parent_id: {name:"Select category", rule:"required|number"},
	category_name: {name:"Sub Category", rule:"required|string"}
});
valid.css({
	error:{
		inputs: "error_input",
		msg: 		"text-danger"
	},
	success:{
		inputs: "success_input"
	}	
});
valid.run();



let cat_save = document.getElementById("cat_save");
cat_save.onclick = function (e) {
	//Custom validation
	let add_category_form = new V('add_category');
	add_category_form.setRules({
		category_name: {name:"Category", rule:"required|string|max:24|min:1", error_class:"error_input"}
	});
	let err_el = add_category_form.msgElement("err_msg_category_input");
	add_category_form.validation(function (status) {
		if(status.error){
			e.preventDefault();
			err_el.innerHTML=status.error.msg;
		} 
	});
}

  let select_category = document.getElementById("select_category");
  let select_sub_category = document.getElementById("select_sub_category");

  let params = {
		'container':'table_sub_categories',
		'db_field':'category_name',
		'get':'getSubCategories',
		counter: 1,
		//'delte_row':false,
		callback: function (container,obj) {
			container.innerHTML += `
			<tr>
        <td>${this.counter++}</td>
        <td>${obj.category_name}</td>
      	<td>
      		<form action="categories/delete" method="post" onsubmit="return confirm('Izbrisati ?');">
      			<input type="hidden" name="csrf_token" value="${csrf_token()}">
      			<input type="hidden" name="_method" value="DELETE">
        		<input type="hidden" name="category_id" value="${obj.id}">
        		<input type="hidden" name="category_name" value="${obj.category_name}">
        		<button type="submit" class="btn btn-sm">
        			<span class="glyphicon glyphicon-trash text-danger"></span>
        		</button>
       		</form>
        </td>         
      </tr>
			`;
		}
	};

  select_category.onchange = function (el) {
    //getSubCategoriesTable(this.value)
    params.id = this.value;
    SubRender.fill(params);
  };

