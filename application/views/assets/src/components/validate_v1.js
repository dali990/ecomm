//window.Validate = function (id='', rules='') {
var Validate = function (id='', rules='') {
	this.rules = rules;
	this.msgElementHTML;
	this.styles = {
		error: {
			inputs: "error_input",
			msg: 		"text-danger"		
		},
		success:{
			inputs: "success_input",
			msg: 		"text-success"	
		},
		custom: false
	};
	var self = this;
	//Constructor
	this.ctor = function (id,rules) {
		if (id != '') {
			this.form = document.getElementById(id);
			this.submit = this.form.querySelector("[type='submit']");
		}

		this.attachDefaultCSS();
		console.log(rules)
	};
	
	this.setRules = function (rules) {
		this.rules = rules;
		//this.BeforeSubmit();
	};

	this.css = function (css) {
		this.styles = css;
	}

	this.run = function (debug=false) {
		let passed = false;
		this.form.onsubmit = function (e) {
			//console.log(e);
			//e.preventDefault();
			for (rule_name in self.rules) {
				let err_elem = e.target.querySelector('#err_msg_'+rule_name);
				let input = e.target.querySelector('[name="'+rule_name+'"]');
				// Debug
				if (!input || input == null || input == 'undefined') {
					e.preventDefault();
					console.log("Cant finde field "+rule_name);
				}
				let validation = self.validating(input.value, self.rules[rule_name].rule);
				//If validation fails
				if (validation.error || debug) {
					console.log("Nije validan "+self.rules[rule_name].name+ ". Greska: "+validation.error.msg);
					passed = false;
					e.preventDefault();
					//If the error div is already inserted (from previos errors)
					if (err_elem) {
						err_elem.className = self.styles.error.msg;
						err_elem.innerHTML = validation.error.msg;
						input.classList.remove(self.styles.success.inputs);
						input.classList.add(self.styles.error.inputs);
					} else { //For the first error no need for removing succes classes
						let parentNode = input.parentNode;
						let msg = document.createElement("div");
						msg.id = "err_msg_"+rule_name;
						msg.className = self.styles.error.msg;
						//msg.innerHTML = "Nije validan "+self.rules[rule_name].name+ ". Greska: "+validation.error.msg;
						msg.innerHTML = validation.error.msg;
						parentNode.insertBefore(msg, input);
						//If provided custom error class
						if (self.rules[rule_name].error_class) {
							input.classList.add(self.rules[rule_name].error_class);
						} else {
							input.classList.add(self.styles.error.inputs);
						}						
					}
				//Validation success
				} else {
					passed = true;
					if (err_elem) {
						err_elem.innerHTML="";
						err_elem.className="";
						input.classList.remove(self.styles.error.inputs);
						input.classList.add(self.styles.success.inputs);
					}
				}
			
			}
			// -1 za button submit
			/*for (var i = 0; i < e.srcElement.length-1; i++) {
				let name = e.srcElement[i].name;
				let validation = self.validating(e.srcElement[i].value, self.rules[name]);
				if (validation.error) {
					console.log("Nije validan "+name+ ". Greska: "+validation.error.msg);
				}
			
			}*/
		}
		//if (passed) this.form.submit();
	};

	//Custom handler for validation
	this.validation = function (callback) {
		for (rule_name in self.rules) {
			let input = this.form.querySelector("[name='"+rule_name+"']");
			let validation = self.validating(input.value, self.rules[rule_name].rule);
			//Debug
			if (!input || input == null || input == 'undefined') {
					console.log("Cant finde field "+rule_name);
				}
			//If the validation rule has custom error class,then apply to that input
			if (self.rules[rule_name].error_class) {
				input.classList.add(self.rules[rule_name].error_class);
			} else input.classList.add(this.styles.error.inputs);

			callback(validation);
			validation.error ? "" : this.resetMsgElement(rule_name);
		}
	};

	this.ButtonClick = function () {
		this.submit.onclick = function (e) {
			console.log("Submited");
			console.log(e);
		}
	};

	//Validation proccess for each rule
	this.validating = function (input, rule) {
		let constraints = rule.split("|");
		let Input = input;
		let constrainParam = {};
		let status = {error:false, type:null};
		for (var i = 0; i < constraints.length; i++) {	
			//Adding rules with parameters if exists
			if (constraints[i].search(":") != -1 ) {
				let customCons = constraints[i].split(":");
				//constrainParam[customCons[0]] = customCons[1]; // {max: 64}
				constraints[i] = customCons[0]; //(constraints[i] = max)
				Input = {value: input, param: customCons[1]}
			} 

			if (v.hasOwnProperty(constraints[i])) {
				let result = v[constraints[i]](Input);
				if (result) {
				  status.error = {
						msg: result
					}
					return status;
				}
			}

			/*switch (constraints[i]) {
				case "required":
					if (input == "") {
						status.error = {
							msg: "Empty field | " + constraints[i]
						}
					} 
					if (input == "undefined") {
						console.log("Prazno");
					} 
					break;

				case "string":
					if (typeof input != "string") {
						status.error = {
							msg: "Not a " + constraints[i]
						}
					} 
					break;
				case "number":
					if (isNaN(input)) {
						status.error = {
							msg: "Not a " + constraints[i]
						}	
						return status;				
					} 
					break;
				case "max":
					if (input.length > constrainParam.max) {
						status.error = {
							msg: " To many characters, max: " + constrainParam.max
						}	
						return status;				
					} 
					break;
				case "min":
					if (input.length < constrainParam.max) {
						status.error = {
							msg: " Min characters: " + constrainParam.min
						}	
						return status;				
					} 
					break;
				default:
					return {
						error: false
					}
					break;
			}*/
		}
		return status;
		//console.log("Validating "+input+" - "+rule);
	};
	//Validating custom data
	this.check = function (param) {
		if(param.constructor === Array){
			let status = [];
			for (var i = param.length - 1; i >= 0; i--) {
				let validation = this.validating(param.value, param.rule);
				//If validation fails
				if (validation.error) {
					status[param.name] = {
						error: {
							msg: validation.error.msg,
							vmsg: "Error on "+param.name+" field. "+validation.error.msg
						},
						name: param.name
					}
				}
			}
			return status;
		}
		let validation = this.validating(param.value, param.rule);
		if (validation.error) {
			let status = {
				error: {
					msg: validation.error.msg,
					vmsg: "Error on "+param.name+" field. "+validation.error.msg
				},
				name: param.name
			}
			return status;
		} 
		return true;
	}

	//Register custom message element
	this.msgElement = function (id) {
		this.msgElementHTML = this.form.querySelector("#"+id);
		return this.msgElementHTML;
	}

	//Resets the message elements
	this.resetMsgElement = function (input_name="") {
		if(this.msgElementHTML){
			this.msgElementHTML.innerHTML="";
		}
		if (input_name) {
			this.form[input_name].classList.remove(this.rules[input_name].error_class);
		}
	},

	this.attachDefaultCSS = function () {
		let code = `
			.error_input{
			    border: 1px solid red;
			}
			.text-danger{color:red;}
			.tex-success{color:lightgreen;}
			.success_input{
			    border: 1px solid green;
			}
		`;
		let style = document.createElement('style');
    style.type = 'text/css';

    if (style.styleSheet) {
        // IE
        style.styleSheet.cssText = code;
    } else {
        // Other browsers
        style.innerHTML = code;
    }

    document.head.appendChild( style );
	}


	this.ctor(id,rules);
}

const v = {
	required: function(input){
		//console.log("R "+input);
		if (input == "") {
			return "Empty field | required"; 		
		} return null;
	},
	string: function(input){
		//console.log("S "+input);
		if (typeof input != "string") {
			return "Not a string";
		} 
		if (/^[<>]+$/.test(input)) {
			return "Forbidden characters."
		}
		  return null;
	},
	number: function(input){
		//console.log("N "+input);
		if (isNaN(input)) {
			return "Not a number";
		} return null;
	},
  email: function(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(email.toLowerCase())){
    	return "Invalid email";
    } return null;
  },
	max: function(input){
		/*console.log("MAX ");
		console.log(input);*/
		if (input.value.length > parseInt(input.param)) {
			return "To many characters, max: "+input.param;
		} return null;
	},
	min: function(input){
		if (parseInt(input.value.length) < parseInt(input.param)) {
			return "Min " +input.param+" characters."
		} return null;
	},
	max_num: function(input){
		/*console.log("max_num ");
		console.log(input);*/
		if (parseInt(input.value) > parseInt(input.param)) {
			return "To big number, max: "+input.param;
		} return null;
	},
	min_num: function(input){
		/*console.log("max_num ");
		console.log(input);*/
		if (parseInt(input.value) < parseInt(input.param)) {
			return "Number must be greater than: "+(parseInt(input.param)-1);
		} return null;
	},
	array_check: function (input) {
		console.log(input);
	}
}

module.exports = Validate;