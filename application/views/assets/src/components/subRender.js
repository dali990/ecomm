//require("../config.js");
//const APP = require("../config.js");
//import * as APP from '../config';
import {APP} from '../config'; //Import specific object
//import APP from '../config'; //Imports the default or the module.exports

let SubRender = {
	//Selects and inserts all subcategories
	getSubCategories: function (id, select_sub_category) {
		//Removing all options except the first one
		select_sub_category.options.length = 1;

		AJAX.post({
			url: APP.base_url+'ajax/addproduct/getSubCategories/'+id,
			//responseType: 'text'
		})
		.then(function (subs) {
			subs.forEach(function (sub) {
				let option = document.createElement("option");
			  option.text = sub.category_name;
			  option.value = sub.id;
			  select_sub_category.appendChild(option);
			})
		})
	},
	/**
	 * Fills in the table with requested data
	 * @param  {[type]} param [description]
	 * @return {[type]}       [description]
	 * @usage : 
	 * params = {
	 * 	get:'getColors',table:'table_id',id:1,db_field:'color_name',delte_row:false
	 * };
	 * SubRender.fillTable(params)
	 */
	fillTable(param){
		let table = document.getElementById(param.table);
		let counter = 1;
		let url_param = "";
		if (param.id) {
			url_param = param.sub_id ? param.id+"/"+param.sub_id : param.id;
		} 
		if (param.action != "undefined") {
			switch (param.action) {
				case "delete":
					param.method="DELETE";
					break;
				case "update":
					param.method="PUT";
					break;
				default:
					param.method="GET";
					break;
			}
		} else {param.action = "";param.method="GET";}
		AJAX.post({
			url: APP.api_url+param.get+'/'+url_param,
			//csrf: "csrf_test_name",
			//responseType: 'text'
		})
		.then(function (objects) {
			// Clears the table
			table.innerHTML = "";
			objects.forEach(function (obj) {
				table.innerHTML += `
					<tr>
	          <td>${counter++}</td>
	          <td>${obj[param.db_field]}</td>
	        	<td>
	        		<form action="${param.action}" method="post" onsubmit="return confirm('Izbrisati ?');">
	        			<input type="hidden" name="csrf_token" value="${csrf_token()}">
	        			<input type="hidden" name="_method" value="${param.method}">
	          		<input type="hidden" name="delete_${param.db_field}" value="${obj.id}">
	          		<input type="hidden" name="category_name" value="${obj[param.db_field]}">
	          		<button type="submit" class="btn btn-sm">
	          			<span class="glyphicon gglyphicon glyphicon-trash text-danger"></span>
	          		</button>
	         		</form>
	          </td>         
	        </tr>
				`;
			})
		})
	},

	fill(param){
		let container = document.getElementById(param.container);
		let counter = 1;
		let url_param = "";
		if (param.id) {
			url_param = param.sub_id ? param.id+"/"+param.sub_id : param.id;
		} 

		AJAX.post({
			url: APP.api_url+param.get+'/'+url_param,
			//csrf: "csrf_test_name",
			//responseType: 'text'
		})
		.then(function (objects) {
			// Clears the container
			container.innerHTML = "";
			objects.forEach(function (obj) {
				param.callback(container,obj);
			})
			//container.innerHTML += `<hr><div class="pull-right"><legend><i>Total:${objects.length}</i></legend></div>`;
		})
	},

	customFill(param){ //console.log(APP.base_url);
		let table = document.getElementById(param.table);
		let counter = 1;
		let url_param = "";
		if (param.id) {
			url_param = param.sub_id ? param.id+"/"+param.sub_id : param.id;
		} 
		AJAX.post({
			url: APP.api_url+param.get+'/'+url_param,
			//csrf: "csrf_test_name",
			//responseType: 'text'
		})
		.then(function (objects) {
			// Clears the table
			table.innerHTML = "";
			objects.forEach(function (obj) {
				param.callback(obj);
			})
		})
	},

};

export default SubRender;

//Ne moze zajedno import i module.exports