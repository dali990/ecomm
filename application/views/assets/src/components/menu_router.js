/* Manages and shows the selected menu and submenus */
let url_path = window.location.pathname.replace(/\/$/, '');
//Url parts
let URL      = url_path.split("/");
//Controller name (Product)
let menu     = URL[URL.length-2].toLowerCase();
//Method name
let sub_menu = URL[URL.length-1].toLowerCase();

//Menu selection
let selectedMenu = document.getElementById(sub_menu);
if (selectedMenu) {selectedMenu.classList.add('active-menu')}

//Submenu selection Product Settings
if (menu == 'properties') {
    //li element with multi level submenus
    let product_settings_menu = document.getElementById("properties");
    product_settings_menu.classList.add('active');
    product_settings_menu.children[0].classList.add('active-menu-top');
    product_settings_menu.children[1].classList.add('collapse');
    product_settings_menu.children[1].classList.add('in');
    //li tags (li.product_settings_menu>ul>li)
    let subs = product_settings_menu.children[1].children;
    
    if (sub_menu == 'categories') {
        subs[0].children[0].classList.add('active-menu'); //a tag
    }
    if (sub_menu == 'sizes') {
        subs[1].children[0].classList.add('active-menu');
    }
    if (sub_menu == 'colors') {
        subs[2].children[0].classList.add('active-menu');
    }
        
}