<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
</head>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<body>

	<div class="container " style="margin-top: 20px">
		<?php 
		if(isset($msg))
			echo $msg;
		 ?>
		<div class="well " style="width: 500px; margin: 0 auto">
			<?php echo validation_errors(); ?>
			<hr>
			<?php printr($error_fields);?>
			<form action="<?=base_url('home/get');?>" method="POST">
			  <div class="form-group">
			    <label for="exampleInputEmail1">Email address</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="<?=set_value('email');?>">
			    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			  </div>
			  <div class="form-group">
			    <label for="usern">Username</label>
			    <input type="text" class="form-control" id="usern" name="username" value="<?=set_value('username');?>">
			  </div>
			  
			  <button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>

</body>
</html>