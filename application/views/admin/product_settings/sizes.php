<?php include APPPATH.'views/admin/include/head.php'; ?>


    <!-- /. NAV TOP  -->
 <?php include APPPATH.'views/admin/include/menu.php'; ?>

<div class="panel panel-info">
  <div class="panel-heading">
    Dodaj velicine
  </div>

  <div class="panel-body" id="sizes_vue">
    <div class="row">
      <form action="sizes/store" id="add_sizes" method="POST">

        <div class="col-md-4 col-sm-4 col-xs-12 right-hr">
          <input type="hidden" name="csrf_token" value="<?=$csrf_token;?>">       
          <div class="form-group">
            <label>Izaberi kategoriju</label>
            <span class="text-danger"><?php echo form_error('category_id'); ?></span>
            <select name="category_id" class="form-control" id="select_category">
              <option disabled selected=""></option>
              <?php foreach ($sql_categories as $key => $value): ?>
                <option value="<?=$value->id;?>"><?=$value->category_name;?></option>
              <?php endforeach ?>
            </select>
          </div>   
          <div class="form-group">
            <label for="category">Podkategorija: </label>
            <span class="text-danger"><?php echo form_error('sub_category'); ?></span>
            <select name="sub_category" class="form-control" id="select_sub_category">
              <option value=""></option>  

            </select>
          </div><br>
          <input type="submit" name="save_size" class="btn btn-info" value="Snimi">          
        </div>
        <div class="col-md-8 col-sm-8 col-xs-12">   
        <br>   
            <div class="form-group col-md-2 col-sm-4 col-xs-4">
              <input class="form-control" name="size[]" style="padding: 3px">
            </div>
            <div class="form-group col-md-2 col-sm-4 col-xs-4">
              <input class="form-control" name="size[]" style="padding: 3px">
            </div>
            <div class="form-group col-md-2 col-sm-4 col-xs-4">
              <input class="form-control" name="size[]" style="padding: 3px">
            </div>
            <div class="form-group col-md-2 col-sm-4 col-xs-4">
              <input class="form-control" name="size[]" style="padding: 3px">
            </div>
            <div class="form-group col-md-2 col-sm-4 col-xs-4">
              <input class="form-control" name="size[]" style="padding: 3px">
            </div>
            <div class="form-group col-md-2 col-sm-4 col-xs-4">
              <input class="form-control" name="size[]" style="padding: 3px">
            </div>
            <div id="insert_more_sizes" ref="insert_more_sizes" >
              <!-- Za prenos podataka, props -->
              <vuec v-for="n in range" v-bind:sizes="sizes"></vuec>
            </div>
            <div class="form-group col-md-12">
              <button type="button" class="btn btn-success" v-on:click="addSizes()" onclick="addSizes()">
                <span class="glyphicon glyphicon-plus "></span>
              </button>
            </div>
          </div>

        </form>
      </div>
      <hr>
      <div class="row">            
          <!--    Kategorije -->
        <div class="col-md-4">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr class="info">
                  <th>#</th>
                  <th>Naziv</th>
                  <th> </th>
                </tr>
              </thead>
              <tbody id="table_sizes">                        
                
                <tr><td colspan="3"><label>Choose category.</label></td></tr>                                
              </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>


  <?php include APPPATH.'views/admin/include/footer.php'; ?>
  <script src="<?=base_url();?>web/dist/product_settings/sizes.js"></script>
<!--   <script src="<?=base_url();?>web/dist/validateOrg.js"></script> -->

  <script type="text/javascript">
    /*window.V = new Validate('add_sizes');
      V.setRules({
        category_id: {name:"Select category", rule:"required|number"},
        sub_category: {name:"Sub Category", rule:"required|number"},
        size: {name:"Size", rule:"array_check:string", array:true}
      });
    V.run();*/
  </script>
