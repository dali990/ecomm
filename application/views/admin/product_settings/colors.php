<?php include APPPATH.'views/admin/include/head.php'; ?>

<link href="<?=base_url();?>web/flex.css" rel="stylesheet" />

    <!-- /. NAV TOP  -->
<?php include APPPATH.'views/admin/include/menu.php'; ?>

<div class="panel panel-info">
  <div class="panel-heading">
    Boje
  </div>

  <div class="panel-body">
    <div class="row">
      

      <div class="col-md-3 col-sm-5 col-xs-12 ">
        <form action="colors/store"  method="POST" id="add_color" >
          <input type="hidden" name="csrf_token" value="<?=$csrf_token;?>">        
          <div class="form-groupd">
            <label for="category">Boja: </label>
            <input class="form-control" type="text" name="color_name">
          </div><br>
          <input type="submit" name="save_color" class="btn btn-info" value="Snimi">
        </form>
        <hr>      
      </div>

        <!--    Boje -->
      <div class="col-md-9 col-sm-7 col-xs-12 ">
        <!-- <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr class="info">
                <th>#</th>
                <th>Naziv</th>
                <th> </th>
              </tr>
            </thead>
            <tbody id="table_colors">                        
              
              <tr><td colspan="3"><label>No colors.</label></td></tr>   

            </tbody>
          </table>
        </div> -->
        <div class="flex-container" id="table_colors">
          <legend>No colors.</legend>
        </div>
      </div> 

    </div>
    
    <div class="row">            
      
    </div>
  </div>

  <?php include APPPATH.'views/admin/include/footer.php'; ?>
  <script src="<?=base_url();?>web/dist/product_settings/colors.js"></script>

  <script type="text/javascript">
    
  </script>
