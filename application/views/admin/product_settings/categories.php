<?php include APPPATH.'views/admin/include/head.php'; ?>


    <!-- /. NAV TOP  -->
 <?php include APPPATH.'views/admin/include/menu.php'; ?>


  <div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="panel panel-info">
        <div class="panel-heading">
          Kategorije
        </div>
        <div class="panel-body">
          <?php #echo form_open_multipart('admin010/settings');?>       
          <form action="categories/store" id="add_category" method="POST" > 
             <input type="hidden" name="csrf_token" value="<?=$csrf_token;?>">
             <div class="form-group">
              <label>Dodaj novu kategoriju</label>
              <div id="err_msg_category_input" class="text-danger"></div>
              <input class="form-control" type="text" name="category_name">
              <p class="help-block ajax_velicina"></p>
             </div>      
            <input type="submit" name="save_category" class="btn btn-info" id="cat_save" value="Snimi">
          </form>               
        </div>                     
      </div>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr class="info">
              <th>#</th>
              <th>Naziv</th>
              <th> </th>
            </tr>
          </thead>
            <tbody>                        
              <?php $i=1; foreach ($sql_categories as $key => $value): ?>
                <tr>
                  <td><?=$i;?></td>
                  <td><?=$value->category_name;?></td>
                  <td>
                    <form action="categories/delete" method="post" onsubmit="return confirm('Izbrisati ?');">
                      <input type="hidden" name="csrf_token" value="<?=$csrf_token;?>">
                      <input type="hidden" name="_method" value="DELETE">
                      <input type="hidden" name="category_id" value="<?=$value->id;?>" >
                      <input type="hidden" name="category_name" value="<?=$value->category_name;?>" >
                      <button type="submit" class="btn btn-sm">
                        <span class="glyphicon glyphicon-trash text-danger"></span>
                      </button>
                    </form>
                  </td>
                </tr>
              <?php $i++; endforeach ?>                                
            </tbody>
        </table>
      </div>
    </div>
  </div><!--/.ROW-->
  <!--    Pod kategorije -->
  <div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="panel panel-info">
        <div class="panel-heading">
          Pod kategorije
        </div>
        <div class="panel-body">   
          <?php # echo form_open('admin010/settings');?>    
          <form action="categories/store_subcategory" id="validation" method="POST"> 
            <input type="hidden" name="csrf_token" value="<?=$csrf_token;?>"> 
            <div class="form-group">
              <label>Dodaj podkategorije</label>
              <select class="form-control" id="select_category" name="parent_id">
                <option value="" hidden>Izaberi kategoriju</option>
                <?php foreach ($sql_categories as $key => $value): ?>
                  <option value="<?=$value->id;?>"><?=$value->category_name;?></option>
                <?php endforeach ?> 
              </select>
              <label>Nova podkategorija</label>
              <span class="text-danger"><?=form_error("category_name");?></span>
              <input class="form-control" type="text" name="category_name">
              <p class="help-block ajax_velicina"></p>
            </div>      
            <input type="submit" name="save_subcategory" class="btn btn-info" value="Snimi">
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-8 col-sm-8 col-xs-12">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr class="info">
              <th>#</th>
              <th>Naziv</th>
              <th> </th>
            </tr>
          </thead>
          <tbody id="table_sub_categories">                        
            
            <tr><td colspan="3"><label>Choose category.</label></td></tr>
            <!-- <?php $i=1; foreach ($sub_categories as $key => $value): ?>
              <tr>
                <form action="" method="post" onsubmit="return confirm('Izbrisati ?');">
                  <td><?=$i;?></td>
                  <td><?=$value->name;?></td>
                  <td><button type="submit"><span class="glyphicon glyphicon-remove-sign"></span></button>
                  <input type="hidden" name="deleteID" value="<?=$value->id;?>">
                  <input type="hidden" name="table" value="velicina">
                  </td>
                </form>
              </tr>
            <?php $i++; endforeach ?>  -->
                                           
          </tbody>
        </table>
      </div>
    </div>
  </div>

        
<?php include APPPATH.'views/admin/include/footer.php'; ?>
<script src="<?=base_url();?>web/dist/product_settings/categories.js"></script>
<script type="text/javascript">
  /*var validation = document.getElementById("validation"); 

  validation.querySelector("button").onclick = function () {
    let sel = validation.querySelector("select[name='select_category']");
    sel.style.border = '1px solid red';
    console.log(sel.value);
  };*/


</script>