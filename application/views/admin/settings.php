<?php include 'include/head.php'; ?>


    <!-- /. NAV TOP  -->
 <?php include 'include/menu.php'; ?>

          <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
               <div class="panel panel-info">
                  <div class="panel-heading">
                     Dodaj novu boju
                  </div>
                  <div class="panel-body">
                      <?php echo form_open_multipart('admin010/settings');?>
                        <div class="form-group">
                            <label>Boja</label>
                            <input class="form-control" type="text" name="boja">
                            <p class="help-block ajax_boja"></p>
                        </div>
                
                            
                           
                        <button type="submit" name="snimiboju" class="btn btn-info">Snimi </button>

                      </form>
                  </div>
                </div>
                            </div>
              <div class="col-md-4 col-sm-4 col-xs-12">
               <div class="panel panel-success">
                  <div class="panel-heading">
                    Dodaj novu veličinu
                  </div>
                    <div class="panel-body">
                        <?php echo form_open_multipart('admin010/settings');?>       
                           <div class="form-group">
                            <label>Veličina</label>
                            <input class="form-control" type="text" name="velicina">
                            <p class="help-block ajax_velicina"></p>
                           </div>      
                          <button type="submit" name="snimivelicinu" class="btn btn-success">Snimi </button>
                        </form>
                      </div>
                  </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12">
               <div class="panel panel-info">
                  <div class="panel-heading">
                    Kategorije
                  </div>
                    <div class="panel-body">
                        <?php echo form_open_multipart('admin010/settings');?>       
                           <div class="form-group">
                            <label>Dodaj novu kategoriju</label>
                            <input class="form-control" type="text" name="kategorija">
                            <p class="help-block ajax_velicina"></p>
                           </div>      
                          <button type="submit" name="snimikategoriju" class="btn btn-info">Snimi </button>
                        </form>
                        <hr>
                      <?php echo form_open_multipart('admin010/settings');?>       
                           <div class="form-group">
                            <label>Dodaj podkategorije</label>
                            <select class="form-control">
                              <option disabled selected="">Izaberi kategoriju</option>
                            </select>
                            <label>Nova podkategorija</label>
                            <input class="form-control" type="text" name="kategorija">
                            <p class="help-block ajax_velicina"></p>
                           </div>      
                          <button type="submit" name="snimikategoriju" class="btn btn-info">Snimi </button>
                        </form>
                      </div>
                      
                  </div>
                </div>
        </div>
             <!--/.ROW-->
             <div class="row">
             <!-- Boje -->
               <div class="col-md-4">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead >
                                <tr class="info">
                                    <th>#</th>
                                    <th>Naziv</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php $i=1; foreach ($query_boje as $key => $value): ?> 
                                <tr>
                                  <form action="" method="post" onsubmit="return confirm('Izbrisati celu kolonu?');">
                                    <td><?=$i;?></td>
                                    <td><?=$value->name;?></td>
                                    <td><button type="submit" class="btn btn-default btn-m"><span class="glyphicon glyphicon-remove-sign"></span></button>
                                    <input type="hidden" name="deleteID" value="<?=$value->id;?>">
                                    <input type="hidden" name="table" value="boja">
                                    </td>
                                  </form>
                                </tr>
                              <?php $i++; endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <!-- Velicine -->
                <div class="col-md-4">

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="success">
                                    <th>#</th>
                                    <th>Naziv</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php $i=1; foreach ($query_velicine as $key => $value): ?>
                                  <tr>
                                    <form action="" method="post" onsubmit="return confirm('Izbrisati ?');">
                                    <td><?=$i;?></td>
                                    <td><?=$value->name;?></td>
                                    <td><button type="submit"><span class="glyphicon glyphicon-remove-sign"></span></button>
                                    <input type="hidden" name="deleteID" value="<?=$value->id;?>">
                                    <input type="hidden" name="table" value="velicina">
                                    </td>
                                  </form>
                                  </tr>
                                <?php $i++; endforeach ?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
             
          <!--    Kategorije -->
               <div class="col-md-4">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr class="info">
                                    <th>#</th>
                                    <th>Naziv</th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <?php $i=1; foreach ($query_kategorije as $key => $value): ?>
                                  <tr>
                                    <form action="" method="post" onsubmit="return confirm('Izbrisati ?');">
                                    <td><?=$i;?></td>
                                    <td><?=$value->name;?></td>
                                    <td><button type="submit"><span class="glyphicon glyphicon-remove-sign"></span></button>
                                    <input type="hidden" name="deleteID" value="<?=$value->id;?>">
                                    <input type="hidden" name="table" value="velicina">
                                    </td>
                                  </form>
                                  </tr>
                                <?php $i++; endforeach ?>                                
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
   <?php include 'include/footer.php'; ?>
   <script src="<?=base_url();?>web/dist/categories.js"></script>