﻿<?php include 'include/head.php'; ?>

    <!-- /. NAV TOP  -->
 <?php include 'include/menu.php'; ?>
    <!-- /. NAV SIDE  -->
                
  <!--/.Row-->
  <hr />
  <div class="row">
    <div class="col-md-12">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>#</th>
              <th>Naziv</th>
              <th>Cena </th>
              <th>Cena sa popustom</th>
              <th>Opis</th>
              <th>Kolicina</th>
              <th>Kategorija</th>
              <th>Boje</th>
              <th>Velicine</th>
              <th></th>
            </tr>
          </thead>
            <tbody>
              <?php foreach ($sql_products as $key => $value): ?>
                <?php 
                  $ID = $value->id;
                  # Kolicina boja
                  $BK = array();
                  # Kolicina velicina
                  $VK = array();

                  $colors = explode(",", $value->color);
                  $quantity_color = explode(",", $value->quantity_color);
                  for ($i=0; $i < count($colors) ; $i++) { 
                    $BK[] = $colors[$i].'('.$quantity_color[$i].')';
                  }

                  $sizes = explode(",", $value->size);
                  $quantity_size = explode(",", $value->quantity_size);
                  for ($i=0; $i < count($sizes) ; $i++) { 
                    $VK[] = $sizes[$i].'('.$quantity_size[$i].')';
                  }
                  $chunkVK = array_chunk($VK, 2);
                  
                 ?>
                <tr>
                  <td>
                    <?=$value->id;?>
                  </td>
                  <td>
                    <?=$value->name;?>
                  </td>  
                  <td>
                    <?=$value->price;?>
                  </td>
                  <td>
                    <?=$value->discount_price;?>
                  </td> 
                  <td>
                    <?=$value->description;?>                             
                  </td>
                  <td><?=$value->total;?></td>
                  <td><?=$value->category;?></td>
                  <td>
                    <?php 


                      if (!empty($BK)) {
                        for ($i=0; $i <count($BK) ; $i++) { 
                          echo $BK[$i].' ';
                          if (($i+2 % 2) == 0) {
                            echo "<br>";
                          }
                        }
                      }
                      
                     ?>
                  </td>
                  <td>
                    <?php 

                    foreach ($chunkVK as $key => $value) {  ?>
                      <div class="row">
                        <?php foreach ($value as $VK ): ?>
                         <div class="col-sm-6" style="margin-right: -25px;"><?=$VK;?> </div>
                        <?php endforeach ?>
                      </div>

                    <?php }

                      /*if (!empty($VK)) {
                        for ($i=0; $i <count($VK) ; $i++) { 
                          echo $VK[$i].' ';
                          if (($i % 2) == 0) {
                            echo "<br>";
                          }
                        }
                      }*/
                      
                     ?>
                  </td>
                  <td>
                    <form action="edit/<?=$ID;?>" method="POST"> 
                      <input type="hidden" name="csrf_token" value="<?=$csrf_token;?>">
                      <input type="hidden" name="_method" value="PUT">
                      <button class="btn  btn-sm" data-toggle="tooltip" title="Izmeni">
                        <a href="<?=base_url();?>admin010/add/edit/<?=$ID;?>" class="glyphicon glyphicon-pencil"  >  </a>
                        </button>
                    </form>
                    <form action="delete/<?=$ID;?>" method="POST"> 
                      <input type="hidden" name="csrf_token" value="<?=$csrf_token;?>">
                      <input type="hidden" name="_method" value="DELETE">
                      <button class="btn  btn-sm " onclick="return confirm('Izbrisati proizvod?')">
                        <span class="glyphicon glyphicon-trash text-danger" data-toggle="tooltip" title="Izbriši" >  </span>
                      </button>
                    </form>
                  </td>
                </tr>
              <?php endforeach ?>
                
            </tbody>
          </table>
      </div>




      </div>
      
  </div>
  <!--/.Row-->
  <hr />
  
  <!--/.ROW-->


<?php include 'include/footer.php'; ?>
<script src="<?=base_url();?>web/dist/categories.js"></script>
