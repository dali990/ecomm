 <nav class="navbar-default navbar-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="main-menu">
                <li>
                    <div class="user-img-div">
                        <!-- nestavljati # u src salje Request -->
                        <img src="" class="img-thumbnail" />

                        <div class="inner-text">
                            Admin Panel
                        <br />
                            <small>Silluzio Moda</small>
                        </div>
                    </div>

                </li>

                <li>
                    <a  href="<?=base_url();?>admin/product/dashboard" id="dashboard"><i class="fa fa-dashboard "></i>Dashboard</a>
                </li>
            
                <li>
                    <a  href="<?=base_url();?>admin/product/add" id="add"><i class="fa fa-flash "></i>Dodaj proizvod</a>
                    
                </li>
                <li>
                    <a  href="<?=base_url();?>admin/product/settings" id="settings"><i class="fa fa-flash "></i>Podešavanja</a>
                    
                </li>
                 
                  <li>
                    <a href="gallery.html"><i class="fa fa-anchor "></i>Gallery</a>
                </li>
                 <li>
                    <a href="error.html"><i class="fa fa-bug "></i>Error Page</a>
                </li>
                <li>
                    <a href="login.html"><i class="fa fa-sign-in "></i>Login Page</a>
                </li>
                <li  id="properties">
                    <a href="#" ><i class="fa fa-sitemap "></i>Product Properties<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level ">
                        <li>
                            <a href="<?=base_url();?>admin/properties/categories" ><i class="fa fa-bicycle "></i>Categories</a>
                        </li>
                         <li>
                            <a href="<?=base_url();?>admin/properties/sizes" ><i class="fa fa-flask "></i>Sizes</a>
                        </li><li>
                            <a href="<?=base_url();?>admin/properties/colors"><i class="fa fa-flask "></i>Colors</a>
                        </li>
                        <!-- <li>
                            <a href="#">Second Level Link<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="#"><i class="fa fa-plus "></i>Third Level Link</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-comments-o "></i>Third Level Link</a>
                                </li>

                            </ul>

                        </li> -->
                    </ul>
                </li>
               
                <li>
                    <a href="#"><i class="fa fa-square-o "></i>Blank Page</a>
                </li>
            </ul>

        </div>

    </nav>

    <div id="page-wrapper">
        <div id="page-inner">
            <?php if (validation_errors()): ?>
                <div class="row ">
                    <div class="alert alert-danger col-xs-12 text-center validation_errors" >
                        <button class="close" data-dismiss="alert">&times;</button>
                        <p class=" text-center "><?=validation_errors();?></p>
                  </div>
            </div>
            <?php endif ?>
            <div class="row ">
                <div class="col-md-12">
                    <?=(isset($msg))? $msg :'';?>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-12">
                    <h1 class="page-head-line"><?=$pageName;?></h1>
                </div>
            </div> 