
      </div><!-- /. PAGE INNER  -->
    </div><!-- /. PAGE WRAPPER  -->
  </div><!-- /. WRAPPER  -->
  <div id="footer-sec">
      &copy; 2014 YourCompany | Design By : <a href="http://www.binarytheme.com/" target="_blank">BinaryTheme.com</a>
  </div>
  <!-- /. FOOTER  -->
  <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
  <!-- JQUERY SCRIPTS -->
  <script src="<?=base_url();?>web/admin/js/jquery.js"></script>
  <!-- BOOTSTRAP SCRIPTS -->
  <script src="<?=base_url();?>web/admin/js/bootstrap.js"></script>
  <!-- METISMENU SCRIPTS -->
  <script src="<?=base_url();?>web/admin/js/jquery.metisMenu.js"></script>
     <!-- CUSTOM SCRIPTS -->
  <script src="<?=base_url();?>web/admin/js/custom.js"></script>
  
  <script src="<?=base_url();?>web/admin/js/bootstrap-fileupload.js"></script>

  <script>
    $(document).ready(function() {
      $('[data-toggle="tooltip"]').tooltip(); 
      
      $('#podesavanja').click(function() {
          $('#podesavanja').toggleClass('active');
          $('#podesavanja-lista').toggleClass('collapse in collapse');
      });

    });
  </script>

</body>
</html>