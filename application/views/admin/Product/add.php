﻿
<?php include APPPATH.'views/admin/include/head.php'; ?>
<style>
  td{
    padding-right: 15px;
    padding-bottom: 15px;
  }
</style>


<link rel="stylesheet" type="text/css" href="<?=base_url();?>web/admin/css/add_product.css">
    <!-- /. NAV TOP  -->
 <?php include APPPATH.'views/admin/include/menu.php'; ?>
    <!-- /. NAV SIDE  -->
    <?php #echo form_open_multipart($form_action);?>

    <form action="store" enctype="multipart/form-data" method="post" accept-charset="utf-8" id="add_product">
      <input type="hidden" name="csrf_token" value="<?=$csrf_token;?>">
      <div class="panel panel-info">
        <div class="panel-heading">
           <?=$action;?>
        </div>
          <div class="panel-body">
           <div class="row">
            <div class="form-groupd col-md-4">
              <label for="name">Name: </label>
              <input id="name" class="form-control" type="text" name="name" value="">
            </div>
            <div class="form-groupd col-md-4">
              <label for="price">Cena: </label>
              <input id="price" class="form-control" type="text" name="price" value="">
            </div>
            <div class="form-groupd col-md-4">
              <label for="quantity">Ukupna kolicina: </label>
              <input id="quantity" class="form-control" type="text" name="quantity" value="">
            </div>
          </div>
         
         <div class="row">
           <div class="form-groupd col-md-4">
              <label for="discount_price">Cena sa popustom: </label>
              <input id="discount_price" class="form-control" type="text" name="discount_price" value="">
            </div>
            <div class="form-groupd col-md-4">
              <label for="category">Kategorija: </label>
              <select name="category" class="form-control" id="select_category">
                <option value="" disabled selected="">Izaberi kategoriju</option>
                <?php foreach ($sql_categories as $key => $value): ?>
                  <option value="<?=$value->id;?>"><?=$value->category_name;?></option>
                <?php endforeach ?>                              
              </select>
            </div>

            <div class="form-groupd col-md-4">
              <label for="category">Podkategorija: </label>
              <select name="sub_category" class="form-control" id="select_sub_category">
                <option value=""></option>                           
              </select>
            </div>
            
         </div><Br>

         <div class="row">
           <div class="form-group col-md-12 col-xs-12 add_bcg">
              <label for="kolicina">Kolicina velicine: </label>
              <div class="row" id="category_parent"> 
               <!-- input_render -->
              </div> 
            </div>
            <div class="form-group col-md-12 col-xs-12 add_bcg">
              <label for="kolicina">Kolicina boje: </label>
              <div class="row">
                  
                  <?php foreach ($sql_colors as $key ): ?>
                    <div class="col-md-2 col-sm-2 col-xs-4"> <?=$key->color_name;?> 
                    <input class="form-control color" type="text" min="0" name="quantity_color[<?=$key->id;?>]" value="" style="width: 70px;" >
                    </div>
                  <?php  endforeach ?>

             </div>  
            </div>
         </div>

        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="panel panel-info">
            <div class="panel-heading">
               File Uploads
            </div>
            <div class="panel-body">
   
              <div class="form-group">
                <label class="control-label col-lg-4">Slika</label>
                  <div class="">
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                      <span class="btn btn-file btn-default">
                          <span class="fileupload-new">Izaberi sliku</span>
                          <span class="fileupload-exists">Change</span>
                          <input type="file" name="userfile[]" id="fileslika" multiple="">
                      </span>
                      <span class="fileupload-preview"></span>
                      <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                  </div>
                </div>
              </div>
              <div class="form-group">                       
                <div id="dropzone" style="width: 100%; height: 160px;">
                  <label class="control-label col-lg-4">Drop Files</label>
                  <label class="control-label col-lg-12" id="dropzone_msg"></label>
                </div>
                <div class="row" style="border: 1px solid grey; padding: 1px; margin: 5px 0 0 0">
                  <div id="load" style="width: 0%; height: 20px;background-color: #FFC3C3">
                    <span id="count">0%</span>
                  </div>
                </div>
              </div>
    

            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="panel panel-success">
            <div class="panel-heading">
              Opis
            </div>
            <div class="panel-body">
              <textarea class="form-control" name="opis" rows="10" value=""></textarea>
            </div>
          </div>
        </div>
          
      </div>
      <div class="row">
        <div class="col-md-4 form-group">            
          <input type="submit" name="snimi" value="SNIMI" class="btn btn-success">
        </div>      
      </div>
      <div class="row">
        <div class="col-md-4" >
          
        </div>
      </div>
          
    </form>


    
<?php include APPPATH.'views/admin/include/footer.php'; ?>
<script src="<?=base_url();?>web/dist/product/add.js"></script>
<script type="text/javascript">

  AJAX.post({
    data: {name:"Daliboe"},
    url: "http://localhost/ongit/project/ci/ajaxtest.php",
  })
  .then(function (data) {
    console.log(data);
  });


</script>
