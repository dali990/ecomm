<?php 
# Flash messages 
/* To set flash message
		$this->Model_notify->setMessage('Test');
		$this->Model_notify->setError('Error');
	Get the message
		echo $this->Model_notify->getMessage();
		echo $this->Model_notify->errorMessage();
		*/
class m_notify extends CI_Model
{
	protected $error;
	protected $success;

	public function __construct()
	{	
		
		$this->load->library('session');
		$this->error = $this->session->error_flash;
		$this->success = $this->session->success_flash;
		
	}

	public function getMessage()
	{
		if (!empty($this->session->success_flash)) {
			return '<div class="row">
								<div class="alert alert-success col-xs-12 text-center" >
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.$this->session->success_flash.'</p>
							  </div>
				  		</div>';
		  #unset($_SESSION['error_flash']);
		}
		return null;
	}

	public function getError()
	{
		if (!empty($this->session->error_flash)) {
			return '<div class="row">
								<div class="alert alert-danger col-xs-12 text-center">
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.$this->session->error_flash.'</p>
							  </div>
						  </div>';
		  #unset($_SESSION['success_flash']);
		}
		return null;
	}

	public function setError($value='No errors')
	{
		$this->session->set_flashdata('error_flash', $value);
	}

	public function setMessage($value='No message')
	{
		$this->session->set_flashdata('success_flash', $value);
	}

	public function checkErr()
	{
		if ($this->errorMessage() == '') {
			return false;
		} else return true;
	}
}

 ?>