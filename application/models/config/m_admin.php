<?php
defined('BASEPATH') OR exit('No direct script access allowed');

clASs m_admin extends CI_Model {

	public function select($field='', $from='', $where='')
	{
		$this->db->select($field);
		$this->db->from($from);
		if ($where != '') {
			$this->db->where($where);
		}
		#$this->db->where('featured = \'1\'');
		$query = $this->db->get();
		return $query->result();
	}

	public function product($value='')
	{
		$query = $this->db->query('
				SELECT products.id, products.name, price, products.quantity AS total, discount_price, categories.name AS category, description, GROUP_CONCAT(colors.name) AS color, GROUP_CONCAT(product_color.quantity_color) AS quantity_color, GROUP_CONCAT(sizes.name) AS sizes, GROUP_CONCAT(product_size.quantity_size) AS quantity_size
				FROM products
				LEFT JOIN product_color ON product_color.product_id= products.id
				LEFT JOIN colors 				ON colors.id 							 = product_color.color_id
				LEFT JOIN product_size  ON product_size.product_id = products.id
				LEFT JOIN sizes 			  ON sizes.id 							 = product_size.size_id
				LEFT JOIN categories 		ON categories.id 					 = products.category
				WHERE products.id = product_color.product_id AND products.id = product_size.product_id
				GROUP BY products.id 

				 ;
			');
		return $query->result();
	}

	public function product2($value='')
	{
		$query = $this->db->query('
				SELECT GROUP_CONCAT(product_size.quantity_size) AS quantity_size, GROUP_CONCAT(sizes.name) AS size
				FROM products
				
				LEFT JOIN product_size ON product_size.product_id = products.id
				LEFT JOIN sizes 			 ON sizes.id 								= product_size.size_id
				GROUP BY products.id 

				 ;
			');
		return $query->result();
	}

	public function product3($id='')
	{
		$where ='';
		if (!empty($id)) {
			$where = 'WHERE products.id = '.$id;
		}
		$query = $this->db->query('
				SELECT products.id, products.name, price, products.quantity AS total, discount_price, categories.category_name AS category, categories.id AS category_id, description, GROUP_CONCAT(colors.color_name) AS color, GROUP_CONCAT(product_color.quantity_color) AS quantity_color, qs.quantity_size, qs.size


				FROM products
				LEFT JOIN product_color ON product_color.product_id = products.id
				LEFT JOIN colors 			  ON colors.id 							  = product_color.color_id
				LEFT JOIN categories 		ON categories.id 						= products.category
				LEFT JOIN (
										SELECT p.id AS idd, GROUP_CONCAT(product_size.quantity_size) AS quantity_size, GROUP_CONCAT(sizes.size_name) AS size
										FROM products p							
										LEFT JOIN product_size ON product_size.product_id = p.id
										LEFT JOIN sizes 			 ON sizes.id 								= product_size.size_id
										GROUP BY p.id 
									) AS qs on qs.idd = products.id
				'.$where.'

				GROUP BY products.id 

				 ;
			');
		return $query->result();
	}

	public function insertProduct($data)
	{
		$this->db->insert("products", $data['product']);
		$product_id = $this->db->insert_id();

		//$this->db->where('product_id', $product);
		foreach ($data['product_size'] as $key => $value) {
			$this->db->insert("product_size", [
				//Zbog uspostavljenje relacije, sql sam prepoznaje auto inc. id iz product tabele
				"product_id"  => $product_id, 
				"size_id"		  => $key,
				"quantity_size"=>$value
			]);
		};
		foreach ($data['product_color'] as $key => $value) {
			$this->db->insert("product_color", [
				"product_id"  => $product_id, 
				"color_id"		  => $key,
				"quantity_color"=>$value
			]);
		}
	}

	public function insert($table, $data, $id = '')
	{
		/*$data = array(
        'title' => $title,
        'name' => $name,
        'date' => $date
		);*/
		if ($id != '') {
			$this->db->where('id', $id);
			return $this->db->update($table, $data);
		} else {
			return $this->db->insert($table, $data);
		}

	}

	public function deleteRow($table, $id)
	{
		$this->db->where('id', $id);
		return $this->db->delete($table);
	}
	
	public function insertColumn($table, $column)
	{
		return $this->db->query('ALTER TABLE `'.$table.'`  ADD `'.$column.'` TEXT NULL');
	}

	public function removeColumn($table, $column)
	{
		return $this->db->query('ALTER TABLE `'.$table.'`  DROP `'.$column.'`');
	}
}
