<?php 

use PHPUnit\Framework\TestCase;
use Test\controllers\Product;

define("BASEPATH", "http://localhost/ongit/project/ci/");
/**
* 		
*/
class productControllerTest extends TestCase
{
	private $http;

  public function setUp()
  {
      $this->http = new GuzzleHttp\Client(['base_uri' => BASEPATH]);
  }
  public function tearDown() {
      $this->http = null;
  }
	public function testIndexMethod($value='')
	{
		$r = new Product();
		//$this->assertEquals($r->index(), "OK");

		$response = $this->http->request("GET", "admin/productsettings");
		$content = $response->getBody()->getContents();
		$this->assertContains("categories", $content);
	}

	public function testSaveNewColor()
	{
		$response = $this->http->request('POST', 'admin/productsettings/saveColor', [
	    'form_params' => [
	      'color_name' => 'Red @',
	      'save_color' => 'Save',
	      /*'nested_field' => [
	          'nested' => 'hello'
	      ]*/
	    ]
		]);
		$content = $response->getBody()->getContents();
		$this->assertContains("Pogresan format ", $content);

	}
}