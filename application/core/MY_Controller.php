<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\RestGuard;
//class Add extends MX_Controller {}

class MY_Controller extends CI_Controller {

	protected $selectedLng = '';
	public $popMsg = '';
	
	public function __construct()
	{
		parent::__construct();
		RestGuard::CheckMethod($this->input->post(),$this);

		$this->load->model('m_languages');
		$lang_codes = $this->m_languages->getLanguages();
		#printr($lang_codes);

		$languages = array('EN', 'DE', 'RS');

		if (array_key_exists(strtolower($this->uri->segment(1)), $lang_codes)) {
			# Parameters directory and lang
			#$this->load->language($this->uri->segment(1), $this->uri->segment(1));
			# New load method
			$this->lang->load(strtolower($this->uri->segment(1)), strtolower($this->uri->segment(1)));
			# Current language on page
			$this->selectedLng = $this->uri->segment(1);
			
		} else $this->selectedLng = 'en';
		define("LNG", $this->selectedLng);
		#$this->session->set_userdata(array("lang"=>$this->selectedLng));


		$this->load->model('m_notify');

		if ($this->m_notify->getMessage()) {
			$this->popMsg = $this->m_notify->getMessage();
		} elseif ($this->m_notify->getError()) {
			$this->popMsg = $this->m_notify->getError();
		} #else $this->popMsg = 'NECE';

	}

	public function index()
	{
		#$this->load->view('welcome_message');
	}
}
?>