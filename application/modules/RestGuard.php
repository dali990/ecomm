<?php 
namespace App;
use App\Security;
use App\Notify;
/**
* 	RestGuard implement in the core class
*/
class RestGuard 
{
	protected static $CI = null;
	function __construct()
	{
		
	}
  /**
   * Secures REST calls
   * @param array $POST [Post parameters]
   * @param object $CI  [CI instace]
   */
	public static function CheckMethod($POST,$ci)
	{
    self::$CI = $ci;
		self::$CI->load->library('session');
		//rsegment - Izbacuje segmente sa nazivom root foldera (bez /admin)
		$controller = self::$CI->uri->rsegment(1); 
  	$method     = self::$CI->uri->rsegment(2);
  	$param     = self::$CI->uri->rsegment(3);

    #echo "controller: $controller<br>method: $method<br>"; 
		
    if (strpos($method, "_")) {
      $method = explode("_", $method)[0];
    }
		
    switch ($method) {
    	case 'update':
    		if (!array_key_exists('_method',$POST) || $POST['_method'] !='PUT') {
    			self::$CI->session->set_flashdata('error_flash', "Cant update, missing method!");
    			redirect($_SERVER['HTTP_REFERER']);
    		}  
        self::tokenCheck($POST); 		
    		break;

    	case 'store':
    		if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    			self::$CI->session->set_flashdata('error_flash', "Cant save data, missing method!");
    			redirect($_SERVER['HTTP_REFERER']);
    		}    	
        self::tokenCheck($POST);	
    		break;

    	case 'delete':
        //self::Printr($_POST);die();
    		if (!array_key_exists('_method',$POST) || $POST['_method'] !='DELETE') {
    			self::$CI->session->set_flashdata('error_flash', "Cant delete, missing method!");
    			redirect($_SERVER['HTTP_REFERER']);
    		}
        self::tokenCheck($POST);    		
    		break;

    	case 'show':
    		if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    			self::$CI->session->set_flashdata('error_flash', "Cant access data!");
    			redirect($_SERVER['HTTP_REFERER']);
    		}    		
    		if ($param=="") {
    			self::$CI->session->set_flashdata('error_flash', "Missing ID parameter!");
    			redirect($_SERVER['HTTP_REFERER']);
    		}
        self::tokenCheck($POST);
    		break;
    	
    	default:
    		Notify::Clog("Request OK!");
    		break;
    }

	}

  public static function REST()
  {
    if (!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD'] !== 'POST') {
      header("HTTP/1.1 403 Forbidden");
      die("NO AJAX");
    }
    if (!isset($_SESSION['ajax_request']) || $_SESSION['ajax_request']=="") {
      header("HTTP/1.1 403 Forbidden");
      die("SESS NULL");
    }
  }

  public static function tokenCheck($token)
  {
    if (!Security::csrfCheck($token['csrf_token'])) {
      self::$CI->session->set_flashdata('error_flash', "No token!");
      redirect($_SERVER['HTTP_REFERER']);
    } 
  }

  private static function Printr($value='')
  {
    echo "<pre>";
    print_r($value);
    echo "</pre>";
  }
}
 ?>