<?php 
namespace App;

/**
 * Facade for CI validation
 */
class Validate 
{
  #Custom validation rules
  use ValidationRules;

  protected $ValidationRules = null;
  public static $errorArray = [1,2,3];
  static $CI = null;
  static $Valid = null;

  public static function init()
  {
    self::$CI = &get_instance();
    self::$CI->load->helper('form');
		self::$CI->load->library('form_validation');
    self::$CI->form_validation->set_message('char_plus', 'Not allowed characters');

  }
  # Validating form with custom rules
  public static function form($rule)
  { 
    self::init();
    //printr(self::$CI->input->post());
    
		self::$CI->form_validation->set_rules($rule);
		if (self::$CI->form_validation->run() == FALSE)
    {
    	#self::$CI->index();
      #self::$CI->load->view('home');
      #return false;
      //redirect(base_url().'home');
      return false;
    }
    else
    {	
    	//self::$CI->load->view('home');
      #return true;
      //echo "Validation passed!";
      return true;
    }
  }

  public static function data($data, $rules)
  {
    self::init();
    self::$CI->form_validation->set_data($data);
    self::$CI->form_validation->set_rules($rules);
    if (self::$CI->form_validation->run() == FALSE){
      self::$errorArray = self::$CI->form_validation->error_array();
      return false;
    }

    return true;

  }

  /**
   * Accessing custom validation rules
   * @param string $rule (string,int,url)
   * @param mixed $data 
   */
  # Implemented trait class
  /*public static function ValidateRule($rule, $data)
  {  
    return ValidationRules::check($rule,$data);
  }*/
}



?>