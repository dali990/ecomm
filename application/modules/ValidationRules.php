<?php 
namespace App;
/**
 * 	For use in Validation calss
 */
trait ValidationRules {
										#Stores error after array element fails validation
	static protected 	$arrayMix = [],
										$errorMsg = 'No errors';
	
	public static function setError($value='')
	{
		self::$errorMsg = $value;
		return self::$errorMsg;
	}
	public static function error()
	{
		if (self::$errorMsg != '') {
			return self::$errorMsg;
		}
	}
	/**
	 * Gives errors from the validation array
	 * @return [array/null] [array-when fails on validation/null-when passed]
	 */
	public static function showArrayStatus()
	{
		if (count(self::$arrayMix)>0) {
			return self::$arrayMix;
		}
		return null;
		
	}

	/**
	 * Array validation
	 * @param  [array] $data [Array for validation]
	 * @param  [array/string] $rules  [Rules/rule for validation]
	 * @return [boolean]       [true/false for validation]
	 */
	public static function arrayCheck($data, $rules){
		# Prati status za svaki element
		$status = array();
		$type = '';
		if (is_array($rules)) {
			$type = 'mix';
		} else $type = $rules;
		# Validation for multiple rules
		switch ($type) {
			case 'mix':
				foreach ($data as $data_key => $data_value) {
					foreach ($rules as $data_name => $rule) {
						if ($data_key == $data_name) {
							if (!self::Check($rule, $data_value)){
								self::$arrayMix[$data_name] = self::$errorMsg;
							}						
						}
					}				
				}

				if (count(self::$arrayMix)>0) {
					return false;
				}
				return true;				
				break;

			# Validation for one rule
			default:
				foreach ($data as $data_key => $data_value) {
					if (!self::Check($type, $data_value)){
						self::$arrayMix[$data_key] = self::$errorMsg;
					}
				}

				if (count(self::$arrayMix)>0) {
					return false;
				}
				return true;
				break;
		}
	}

	/**
	 * Validation
	 * @param [string]  $check  [Rule for validation]
	 * @param [mix]  $data  [Data to validate]
	 * @param integer $limit [Total number of characters]
	 */
	public static function Check($check, $data, $limit = 1500)
	{	
		switch ($check) {
			
		case 'string':
				if (preg_match('/^[0-9a-zA-Z*\p{L} ]+$/iu', $data)) {
					if (strlen($data)>24) {						
						self::setError('FAILD sanitizing string 1');
						return false;
					}
					//messageS('Input successful sanatized');
					return filter_var($data, FILTER_SANITIZE_STRING);
				} else { 
					self::setError('FAILD sanitizing string 2');
					return false;
				}
			break;

		case 'string_limit':
				if (preg_match('/^[0-9a-zA-Z\p{L} ]+$/iu', $data)) {
					if (strlen($data)<1 || strlen($data)>$limit) {						
						self::setError('FAILD sanitizing string');
						return false;
					}
					//messageS('Input successful sanatized');
					return filter_var($data, FILTER_SANITIZE_STRING);
				} else { 
					self::setError('FAILD sanitizing string');
					return false;
				}
			break;

			case 'string_0':
				if (preg_match('/^[0-9a-zA-Z\p{L} ]+$/iu', $data)) {
					if (strlen($data)>$limit) {						
						self::setError('FAILD sanitizing string');
						return false;
					}
					//messageS('Input successful sanatized');
					return filter_var($data, FILTER_SANITIZE_STRING);
				} else { 
					self::setError('FAILD sanitizing string');
					return false;
				}
			break;

		case 'int':
				if (preg_match('/^[0-9 ]+$/', $data)) {
					if (strlen($data)>16) {
						self::setError('FAILD sanitizing integer');
						return false;
					}
					//messageS('Input successful sanatized');
					return filter_var($data, FILTER_VALIDATE_INT);
				} else { 
					self::setError('FAILD sanitizing integer');
					return false;
				}
			break;

			case 'int_limit':
				if (preg_match('/^[0-9 ]+$/', $data)) {
					if (strlen($data)<1 || strlen($data)>$limit) {
						self::setError('FAILD sanitizing int_limit, riched max value');
						return false;
					}
					//messageS('Input successful sanatized');
					return filter_var($data, FILTER_VALIDATE_INT);
				} else { 
					self::setError('FAILD sanitizing int_limit');
					return false;
				}
			break;

			case 'int_andEmpty':
			if ($data == '') {
				return '';
			} else{
				if (preg_match('/^[0-9 ]+$/', $data)) {
					if (strlen($data)>$limit) {
						self::setError('FAILD sanitizing integer int_andEmpty');
						return false;
					}
					//messageS('Input successful sanatized');
					return filter_var($data, FILTER_VALIDATE_INT);
				} else { 
					self::setError('FAILD sanitizing integer int_andEmpty');
					return 0;
				}
			}
			break;

		case 'email':
				if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
					if (strlen($data)>31) {
						self::setError('FAILD sanitizing mail');
						return false;
					}
					# filter_var uklanja cirilicu
					return $data;
				} else { 
					self::setError('FAILD sanitizing mail');
					return false;
				}
			break;

			case 'email_limit':
				if (filter_var($data, FILTER_VALIDATE_EMAIL)) {
					if (strlen($data)<1 || strlen($data)>$limit) {
						self::setError('FAILD sanitizing mail');
						return false;
					}
					return $data;
				} else { 
					self::setError('FAILD sanitizing mail');
					return false;
				}
			break;

		case 'address':
				if (preg_match('/^[0-9a-zA-Z*\p{L}\/\-() ]+$/iu', $data)) {
					if (strlen($data)>51) {
						self::setError('FAILD sanitizing address');
						return false;
					}
					return $data;
				} else {  
					self::setError('FAILD sanitizing address');
					return false;
				}
			break;

			case 'address_limit':
				if (preg_match('/^[0-9a-zA-Z*\p{L}\/\-() ]+$/iu', $data)) {
					if (strlen($data)<1 || strlen($data)>$limit) {
						self::setError('FAILD sanitizing address');
						return false;
					}
					return $data;
				} else {  
					self::setError('FAILD sanitizing address');
					return false;
				}
			break;

			case 'text':
				if (preg_match('/^[0-9a-zA-Z\p{L}\/()\'\" ]+$/iu', $data)) {
					if (strlen($data)>200) {
						self::setError('FAILD sanitizing text');
						return false;
					}
					return $data;
				} else { 
					self::setError('FAILD sanitizing text');
					return false;
				}
			break;

			case 'text_limit':
				if (preg_match('/^[0-9a-zA-Z\p{L}\/()\'\" ]+$/iu', $data)) {
					if (strlen($data)<1 || strlen($data)>$limit) {
						self::setError('FAILD sanitizing text');
						return false;
					}
					return $data;
				} else { 
					self::setError('FAILD sanitizing text');
					return false;
				}
			break;

			case 'url':
				if (filter_var($data, FILTER_SANITIZE_URL)) {
					if (strlen($data)>70) {
						self::setError('Max character lenght 70');
						return false;
					}
					//messageS('Input successful sanatized');
					return $data;
				} else { 
					self::setError('FAILD sanitizing url'); 
					return false;
				}
			break;

			case 'json':
				if (preg_match('/^[0-9a-zA-Z\p{L}\/\'\":{}\[\]_,.\\\\ -]+$/iu', $data)) {
					if (strlen($data)>$limit) {
						self::setError('Reached max character lenght for json');
						return false;
					}
					//messageS('Input successful sanatized');
					return $data;
				} else { 
					self::setError('FAILD sanitizing json'); 
					return false;
				}
			break;

			case 'json2':
				if (preg_match('/(*UTF8)^[0-9a-zA-Z\/\'\":{}\[\]+,.&()\\\\ _-]+$/', $data)) {
					if (strlen($data)>$limit) {
						self::setError('Reached max character lenght for json2');
						return false;
					}
					//messageS('Input successful sanatized');
					return $data;
				} else { 
					self::setError('FAILD sanitizing json2'); 
					return false;
				}
			break;

			case 'phone':
				if (preg_match('/^[0-9 \/()-]+$/', $value)) {
					if (strlen($data)>$limit) {
						self::setError('Reached max character lenght for phone');
						return false;
					}
					//messageS('Input successful sanatized');
					return $data;
				} else { 
					self::setError('FAILD sanitizing phone'); 
					return false;
				}
			break;

			case 'search_int':
				if (self::$RegChek_searchInt($data)) {
					if (strlen($data)>$limit) {
						self::setError('Reached max character lenght for search_int');
						return false;
					}
					//messageS('Input successful sanatized');
					return $data;
				} else { 
					self::setError('FAILD sanitizing search_int'); 
					return false;
				}
			break;

		default:
				self::setError('Error in switch case');
				return false;
			break;
		}
		
	}


}
 ?>