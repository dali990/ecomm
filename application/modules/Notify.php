<?php 
namespace App;
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Get and set session flash messages
*/
class Notify 
{
	protected static $CI = null;
	protected static $data = null;
	protected static $error = null;
	protected static $success = null;
	protected static $message = null;
	protected static $errorMsg = null;

	function __construct()
	{
		# code...
	}
	
	function __invoke(){
		return false;
	}
	
	public static function __callStatic($method, $arg){
		self::getInstance();
		self::init();
		return call_user_func_array([__CLASS__,$method], $arg);
	}

	protected static function getInstance(){
		if (self::$CI == null) {
			self::$CI = &get_instance();
		}
	}
	
	protected static function init($value='')
	{		
		if (!empty(self::$CI->session->error_flash)) {
			self::$error = '<div class="row">
								<div class="alert alert-danger col-xs-12 text-center">
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.self::$CI->session->error_flash.'</p>
							  </div>
						  </div>';
		  #unset($_SESSION['success_flash']);
		}
		if (!empty(self::$CI->session->success_flash)) {
			self::$success = '<div class="row">
								<div class="alert alert-success col-xs-12 text-center" >
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.self::$CI->session->success_flash.'</p>
							  </div>
				  		</div>';
		  #unset($_SESSION['error_flash']);
		}
		if (!empty(self::$CI->session->data_flash)) {
			self::$data = unserialize(self::$CI->session->data_flash);
		  #unset($_SESSION['error_flash']);
		}
		return null;
	}

	protected static function hasError($value='')
	{
		if (self::$error != null) {
			return true;
		}
	}
	# Execute callback function if has error
	protected static function ifError($callback)
	{
		if (self::$error != null) {
			$callback(self::$error);
		}
	}

	protected static function ifMessage($callback)
	{
		if (self::$message != null) {
			$callback(self::$message);
		}
	}

	protected static function getError($value='')
	{		
		if (self::$error != null) {
			return self::$error;
		}
		return;
	}

	protected static function getErrorMsg($value='')
	{		
		if (self::$errorMsg != null) {
			return self::$errorMsg;
		}
		return;
	}

	protected static function getSuccess($value='')
	{		
		if (self::$success != null) {
			return self::$success;
		}
		return;
	}

	protected static function getMessage($value='')
	{		
		if(self::getError()) return self::getError();
		if(self::getSuccess()) return self::getSuccess();
		if(self::getErrorMsg()) return self::getErrorMsg();
		return false;
	}

	protected static function getData($value='')
	{		
		if (self::$data != null) {
			return self::$data;
		}
		return;
	}

	public static function getBootstrap()
	{
		echo '<link href="'.base_url().'web/admin/css/bootstrap.css" rel="stylesheet" />';
	}

	public static function setError($value='No errors')
	{
		self::getInstance();
		self::$CI->session->set_flashdata('error_flash', $value);
	}

	public static function setErrorMsg($value='No errors')
	{
		self::$errorMsg = '<div class="row">
								<div class="alert alert-danger col-xs-12 text-center" >
									<button class="close" data-dismiss="alert">&times;</button>
									<p class=" text-center">'.$value.'</p>
							  </div>
				  		</div>';
	}

	public static function setSuccess($value='No message')
	{
		self::getInstance();
		self::$CI->session->set_flashdata('success_flash', $value);
	}

	public static function setData($data=null)
	{
		self::getInstance();
		if (is_array($data)) {
			self::$CI->session->set_flashdata('data_flash', serialize($data));
		} else
		die("Notify::setData() error! Expexting array! Given: ". gettype($data));
	}

	public static function Clog($value='')
	{
		echo "<script>console.log('$value');</script>";
	}
	public static function TestMsg($value='')
	{
		echo "<div style='display:none'>$value</div>";
	}


}

 ?>