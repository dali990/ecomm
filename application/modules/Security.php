<?php 
namespace App;

/**
* 		
*/
class Security 
{
	protected static $CI = null;
	protected static $error = null;
	protected static $message = null;

	function __construct()
	{
		# code...
	}
	
	function __invoke(){
		return false;
	}
	/**
	 * Creates csrf token and sets the session
	 * @return string 
	 */
	public static function csrf():string
	{
		$csrf_token = bin2hex(openssl_random_pseudo_bytes(16));
		$_SESSION['csrf_token'] = $csrf_token;
		//$_SESSION['array'] = " ";
		//$_SESSION['array'] .= " ".$csrf_token;
		return $csrf_token;
	}

	/**
	 * Checking for csrf token and removin session if true
	 * @param  String  $value csrf token code
	 * @return boolean     
	 */
	public static function csrfCheck(string $value):bool
	{
		if (!isset($_SESSION['csrf_token']) && empty($_SESSION['csrf_token'])) {
			return false;
		}
		if ($_SESSION['csrf_token'] != $value) {
			return false;
		}

		unset($_SESSION['csrf_token']);
		return true;
	}

	private static function back()
	{
		die('<script>
					window.history.back(); 
					alter("Teken manipulation");
				</script>');
	}

	public static function csrfReset($value='')
	{
		if (isset($_SESSION['csrf_token']) && !empty($_SESSION['csrf_token'])) {
			unset($_SESSION['csrf_token']);
		}
	}
}

# Hashtag (#) u src atributu img taga salje dupli zahtev koji overajduje csrf token
 ?>