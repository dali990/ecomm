<?php

class Upload extends CI_Controller {

    public function __construct()
    {
      parent::__construct();
      $this->load->helper(array('form', 'url'));
      $this->load->library('upload');
      $this->load->library('image_lib');

      $this->load->helper('file');
    $now = DateTime::createFromFormat('U.u', microtime(true));
    write_file('csrf.txt', $now->format("m-d-Y H:i:s.u")."\n", 'a');
    }

    public function index()
    {


      $this->load->view('welcome_message', array('error' => ' ' ));

    }

    public function do_upload()
    {
      if ($this->input->post('upload') && count($_FILES['userfile']['name'])>0) {

        $filesNo = count($_FILES['userfile']['name']);
         $files = $_FILES;



        #printr($files); die();

        $url = './web/product_images/';
        $name = date('m.d[H.i.s]');
        if (!is_dir($url.$name)) {
          mkdir($url.$name, 0777, true);
          $path = $url.$name;
        }

        
      
        for ($i=0; $i < $filesNo; $i++) { 
          
          $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
          $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
          $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
          $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
          $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
        
          $config['upload_path']          = $path;
          $config['allowed_types']        = 'gif|jpg|png';
          $config['max_size']             = 10000;
          $config['max_width']            = 8000;
          $config['max_height']           = 8000;
          $config['overwrite']            = true;
          $config['remove_space']         = true;

          
          $this->upload->initialize($config);
          $error = '';
          if ( ! $this->upload->do_upload('userfile'))
          {
                  $error = array('error' => $this->upload->display_errors());

                  $this->load->view('upload_form', $error);
          }
          else
          {
                  $data = array('upload_data' => $this->upload->data());        
          } 

          $imgPath = $path.'/'.$_FILES['userfile']['name'];
          $size = getimagesize($imgPath);
          # Ako slika sirine ne prelazi 900px
          if ($size[0] > 900) {

            $ImgConfig['image_library'] = 'gd2';
            $ImgConfig['source_image'] = $path.'/'.$_FILES['userfile']['name'];
            $ImgConfig['width']         = 900;
            $ImgConfig['height']       = 650;
            /*$ImgConfig['wm_text'] = 'Img 2017 ';
            $ImgConfig['wm_type'] = 'text';
            $ImgConfig['wm_font_path'] = './system/fonts/texb.ttf';
            $ImgConfig['wm_font_size'] = '16';
            $ImgConfig['wm_font_color'] = 'ffffff';
            $ImgConfig['wm_vrt_alignment'] = 'bottom';
            $ImgConfig['wm_hor_alignment'] = 'right';
            $ImgConfig['wm_vrt_offset'] = '-30';
            $ImgConfig['wm_hor_offset'] = '-30';
            $ImgConfig['wm_padding'] = '20';*/


            # Za loop resetuje config
            $this->image_lib->clear();
            $this->image_lib->initialize($ImgConfig);

            $this->image_lib->resize();
            #$this->image_lib->watermark();

            #echo $this->image_lib->display_errors();
          }
        }

        if (empty($error)) {
            #$this->load->view('upload_success', $data);
        } else $this->load->view('upload_success', $error);


      }

    }


  }

/*  $config['upload_path'] = 'upload/Main_category_product/';
  $path=$config['upload_path'];
  $config['allowed_types'] = 'gif|jpg|jpeg|png';
  $config['max_size'] = '1024';
  $config['max_width'] = '1920';
  $config['max_height'] = '1280';
  $this->load->library('upload', $config);

  foreach ($_FILES as $fieldname => $fileObject)  //fieldname is the form field name
  {
      if (!empty($fileObject['name']))
      {
          $this->upload->initialize($config);
          if (!$this->upload->do_upload($fieldname))
          {
              $errors = $this->upload->display_errors();
              flashMsg($errors);
          }
          else
          {
               // Code After Files Upload Success GOES HERE
          }
      }
  }*/


?>