<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Security;
use App\Validate;
use App\Notify;

class Product extends MY_Controller {

	public function __construct($id='')
	{ 
		parent::__construct();
		$this->controller_url = "admin/".strtolower(__CLASS__);
		$this->load->library('session');
		$this->load->model('m_home');
		$this->load->model('m_notify');
		$this->load->model('config/m_admin');
		$this->load->helper(array('form', 'url', 'dataviz', 'isempty'));
		$this->load->model('config/m_errorHandler');

		$this->session->set_userdata('ajax_request',"true");
		
	}

	public function index($value='')
	{
		redirect("admin/product/add");
		$this->add();
	}
	public function valid($value='')
	{
		$rule = [
			["field"=>"name", "label"=>"Name", "rule"=>"required|alpha"],
			["field"=>"age", "label"=>"Age", "rule"=>"required|numeric"],
		];
		$data = ["name"=>"2", "age"=>"ad"];
		if (!Validate::data($data,$rule)) {
			printr(Validate::$errorArray);
		}
		
	}

	public function Dashboard()
	{
		$this->load->model('m_languages');

		$featured = $this->m_home->featured();
		#printr($featured);
		#$data = $this->lang->language;
		$data['page'] = 'dash';
		$data['pageName'] = 'DASHBOARD'; 
		$data['lng'] = LNG;
		$data['lngName'] = $this->m_languages->getLangName(LNG);
		$data['sql_products'] = $this->m_admin->product3();

		#printr($data['query_product']);
		#printr($this->m_admin->product2());
		#printr($this->m_admin->product3());
		$data['msg'] = Notify::getMessage();
		$data['csrf_token'] = Security::csrf();
		$this->load->view('admin/dashboard.php', $data);
	}

	public function Add($action='', $ID='')
	{
		$data['page'] = 'add';
		$data['action'] = 'Upis podataka';
		$data['pageName'] = 'Dodaj proizvod'; 
		$data['lng'] = LNG;
		$data['lngName'] = $this->m_languages->getLangName(LNG);
		$data['msg'] = $this->popMsg;
		$data['sql_colors'] = $this->m_admin->select('id, color_name', 'colors');
		$data['sql_sizes'] = $this->m_admin->select('*', 'sizes');
		$data['sql_categories'] = $this->m_admin->select('id, category_name', 'categories', 'parent_id = 0');

		if ($this->input->post('snimi')) { 

				$ID = uniqid();

				$bojeArr=''; $kbArr = ''; $velicinaArr=''; $kvArr='';
				/*foreach ($this->input->post('boja') as $boja) { $bojeArr.=$boja.',';} $boje = rtrim($bojeArr, ',');
				foreach ($this->input->post('kolicina_boje') as $kolicina_boje) { $kbArr.=$kolicina_boje.',';} $kb = rtrim($kbArr, ',');
				foreach ($this->input->post('velicina') as $velicina) { $velicinaArr.=$velicina.',';} $velicina = rtrim($velicinaArr, ',');
				foreach ($this->input->post('kolicina_velicine') as $kolicina_velicine) { $kvArr.=$kolicina_velicine.',';} $kv = ltrim(rtrim($kvArr, ','),',');*/

				$insertData = [
					"product" => [
						"id"						=> $ID,
						"name" 					=> $this->input->post('name'),
						"price" 				=> $this->input->post('price'),
						"quantity" 			=> $this->input->post('quantity'),
						"category"			=> $this->input->post('category'),
						"description"	  => $this->input->post('description'),
						"discount_price"=> $this->input->post('discount_price'),
					]
				];
				foreach ($this->input->post('quantity_size') as $key => $value) {
					if ($value != '') {
						$insertData['product_size'][$key] = $value;
					}
				};
				foreach ($this->input->post('quantity_color') as $key => $value) {
					if ($value != '') {
					$insertData['product_color'][$key] = $value;
					}
				};
				//$this->m_admin->addNewProduct($insertData);
				//printr($insertData); die();

				/*$insertData['price'] = $this->input->post('price');
				$insertData['quantity'] = $this->input->post('quantity');
				$insertData['discount_price'] = $this->input->post('discount_price');
				$insertData['category'] = $this->input->post('category');	*/			
				/*$insertData['boja'] = $boje;
				$insertData['kolicina_boje'] = $kb;
				$insertData['velicina'] = $velicina;
				$insertData['kolicina_velicine'] = $kv;*/
				#$insertData['description'] = $this->input->post('description');

				#echo $insertData['naziv'];
				#printr($kv);
				#printr(array_filter($insertData['kolicina_velicine']));
				#printr($boja);
				#printr($kolBoje);

				# DODAVANJE NOVOG PROIZVODA
				if ($action == 'new') { 
					if ($this->m_admin->insert('products', $insertData)) {
						$this->m_notify->setMessage("Uspešno upisan novi proizvod: ".$insertData['name']);
						$data['msg'] = "USPEŠNO DODAT NOVI PROIZVOD"; 
					} else $this->m_notify->setError('Proizvod: "'.$insertData['name']. '" nije bilo moguće snimiti');
				}
				

			#$dataProduct = $this->input->post();
			#printr($dataProduct);

			#header('Location: '.base_url().'admin010/add');
		} /*elseif ($this->input->post('naziv')=='') {$this->m_notify->setError("Polje 'Naziv' nesme biti prazno");}*/



		# EDIT

		if ($action == 'edit'	) {
			$data['sql_product'] = $this->m_admin->product3($ID)[0];
			$IDedit = $this->input->post('IDedit');
			if ($this->input->post('IDedit')) { $this->m_notify->setMessage("TEEEEEEEEEEESSST"); echo "string";

				/*if ($this->m_admin->insert('product', $insertData, $IDedit)) {
						$this->m_notify->setMessage("Uspešno upisan novi proizvod: ".$insertData['naziv']);
					} else $this->m_notify->setError('Proizvod: "'.$insertData['naziv']. '" nije bilo moguće snimiti');*/
			}
			
		} elseif ($action == 'delete') {
			echo "IZBRIsAN $ID";
		}

		$data['msg'] = Notify::getMessage();
		$data['csrf_token'] = Security::csrf();
		$this->load->view('admin/product/add.php', $data);
	}

	public function settings($subPage='')
	{
			

			if ($this->input->post('boja')) {
				$boja = $this->input->post('boja');
				$data = array('naziv' => $boja);
				if ($this->m_admin->insert('boja', $data)) {
					$this->m_notify->setMessage("Uspešno upisana boja ".$boja);
					header('Location: '.base_url().'admin010/settings');
				}
			}

			if ($this->input->post('velicina')) {
				$velicina = $this->input->post('velicina');
				$data = array('naziv' => $velicina);
				if ($this->m_admin->insert('velicina', $data)) {
					$this->m_notify->setMessage("Uspešno upisana velicina ".$velicina);
					header('Location: '.base_url().'admin010/settings');
				}
			}

			if ($this->input->post('kategorija')) {
				$kategorija = $this->input->post('kategorija');
				$data = array('naziv' => $kategorija);
				if ($this->m_admin->insert('kategorija', $data)) {
					$this->m_notify->setMessage("Uspešno upisana kategorija ".$kategorija);
					header('Location: '.base_url().'admin010/settings');
				}
			}

			if ($this->input->post('deleteID')) {
				$table = $this->input->post('table');
				$ID = $this->input->post('deleteID');
				if ($this->m_admin->deleteRow($table, $ID)) {
					$this->m_notify->setMessage("Uspesno izbrisana ".$table);
					header('Location: '.base_url().'admin010/settings');
				}
			}
			$where = 'parent_id = 0 ';
			$data['sql_boje'] = $this->m_admin->select('id, name', 'colors');
			$data['sql_velicine'] = $this->m_admin->select('id, name', 'sizes');
			$data['sql_kategorije'] = $this->m_admin->select('id, name', 'categories', $where);

			//printr($data['query_kategorije']);

			$data['page'] = 'settings';
			$data['subPage'] = $subPage;
			$data['action'] = 'Upis podataka';
			$data['pageName'] = 'Podešavanja'; 
			$data['msg'] = Notify::getMessage();
			$data['csrf_token'] = Security::csrf();
			$data['lng'] = LNG;
			$data['lngName'] = $this->m_languages->getLangName(LNG);


			$this->load->view('admin/settings.php', $data);

	}

	public function do_upload()
  {

  	if (!isset($_FILES['userfile']) || $_FILES['userfile']['name'][0] == '') {
  		return ;
  	}

  	/* php.ini
  		upload_max_filesize = 1000M ;1GB
			post_max_size = 1000M
		*/
    ini_set('upload_max_filesize','20M');	
			
		$config['upload_path']          = './web/product_images/';
    $config['allowed_types']        = 'gif|jpg|png|mp4';
    $config['max_size']             = 100000000;//100MB
    $config['max_width']            = 8000;
    $config['max_height']           = 8000;

    $this->load->library('upload');

    $ID = uniqid();
    $files = $_FILES;
    $filesNo = count($_FILES['userfile']['name']);

    for ($i=0; $i < $filesNo; $i++) {
    	$config['file_name'] = $ID."_$i";
      $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
      $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
      $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
      $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
      $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

      $this->upload->initialize($config);

      if ( ! $this->upload->do_upload('userfile'))
      {
              $data['error'] = $this->upload->display_errors();
              //$this->load->view('admin/product/add.php', $data);
              Notify::setError($data['error']);	
							redirect($this->controller_url."/add");	

      }
      else
      {
              /*$data['upload_data'] = $this->upload->data();
              printr($data['upload_data']);*/
      }
		}
  }

  /**
	 * Saves new item to database
	 * @return [type]        [description]
	 */
	public function store(Request $r)
	{ 

		$sql_data['product'] = [
				"name" 					=> $this->input->post('name'),
				"price" 				=> $this->input->post('price'),
				"quantity" 			=> $this->input->post('quantity'),
				"category"			=> $this->input->post('category'),
				"description"	  => $this->input->post('description'),
				"discount_price"=> $this->input->post('discount_price'),
		];
		foreach ($this->input->post('quantity_size') as $key => $value) {
			if ($value != '') {
				$sql_data['product_size'][$key] = $value;
			}
		};
		foreach ($this->input->post('quantity_color') as $key => $value) {
			if ($value != '') {
			$sql_data['product_color'][$key] = $value;
			}
		};

		$rule = [
			[
				'field' => 'name',
				'label' => 'Product name',
				'rules' => 'required|alpha_numeric_spaces'
			]
		];
		if(Validate::form($rule)){
			#$this->m_admin->insertProduct($sql_data);
			$this->do_upload();
			Notify::setSuccess("Nova proizvod sacuvana ".$sql_data['product']['name'].".");
			redirect($this->controller_url."/add");
		} else Notify::setError("Pogresan format podataka!");	
		redirect($this->controller_url."/add");		
		

	}

	#New
	public function update($value='')
	{
		printr($this->uri);
	}

	public function show(Request $request)
	{
		printr($request->color_name);
		printr($request->total());
		printr($request->to_array());
	}

	/**
	 * Delete item from database
	 * @return [type] [description]
	 */
	public function delete($ID)
	{
		if(Validate::check('int',$ID)){
			#$this->m_admin->deleteRow('products', $ID);
			Notify::setSuccess("Proizvod izbrisan.$ID");
			redirect($this->controller_url."/dashboard");
		} else Notify::setError("Pogresan format podataka!");	
		redirect($this->controller_url."/dashboard");		

	}

	public function dbtest()
	{
		$data = [
			"name" => "TEST ROBA",
			"price"=> 250
		];
		$d = $this->db->insert("products",$data);
		printr($this->db->insert_id());
	}
    
}
