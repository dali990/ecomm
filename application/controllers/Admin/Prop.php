<?php 
declare(strict_types=1);
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Security;
use App\Validate;
use App\Notify;

class Prop extends MY_Controller {
	protected $controller_url;
	public function __construct($id='')
	{ 
		parent::__construct();
		$this->controller_url = "admin/".__CLASS__."/";
		$this->load->library('session');
		$this->load->model('m_home');
		#$this->load->model('m_notify');
		$this->load->model('config/m_admin');
		$this->load->helper(array('form', 'url', 'dataviz', 'isempty'));
		#$this->load->model('config/m_errorHandler');
		#Security::csrfReset();
		/*$this->load->helper('file');
		$now = DateTime::createFromFormat('U.u', microtime(true));
		write_file('csrf.txt', $now->format("m-d-Y H:i:s.u")."\n", 'a');*/

		/*$data = $this->security->xss_clean("<h2>TEST</h2><script>alert('hack')</script>");
		$sess =  $this->security->get_csrf_hash();
		echo $sess;*/
		//Ajax security
		$this->session->set_userdata('ajax_request',"true");

	}

	public function index()
	{	

		$this->Categories();
		/*echo "Product";
		Notify::getBootstrap();
		echo Notify::getMessage();
		printr(Notify::getData());*/
	}

	public function Categories($value='')
	{	
		$where = 'parent_id = 0 ';
		$data['sql_categories'] = $this->m_admin->select('id, category_name', 'categories', $where);

		# Add new category
		if ($this->input->post("save_category")) {
			$this->test();
			if (Security::csrfCheck($this->input->post('csrf_token'))) {
				$sql_data = [
					'category_name' => $this->input->post('category_name'),
					'parent_id' => 0
				];
				$rule = [
					[
						'field' => 'category_name',
						'label' => 'Category',
						'rules' => 'required|alpha_numeric_spaces'
					]
				];
				if(Validate::form($rule)){
					$this->db->insert("categories", $sql_data);
					Notify::setSuccess("Nova kategorija uspesno sacuvana!");
					redirect($this->controller_url."categories");
				} else Notify::setError("Pogresan format podataka!");
				
			} 
		}
		
		# Delete category
		if ($this->input->post("delete_category")) {
			$ID = $this->input->post("delete_category");
			$category_name = $this->input->post("category_name");
			if (Security::csrfCheck($this->input->post('csrf_token'))) {
				$this->m_admin->deleteRow('categories', $ID);
				Notify::setSuccess("Izbrisano ".$category_name);
				redirect($this->controller_url."categories");
			} 
		}

		# Add new subcategory
		if ($this->input->post("save_subcategory")) {
			printr($this->input->post());
			if (Security::csrfCheck($this->input->post('csrf_token'))) {
				$sql_data = [
					'parent_id' => $this->input->post("parent_id"),
					'category_name' => $this->input->post("category_name")
				];
				$rule = [
					[
						'field' => 'parent_id',
						'label' => 'Category',
						'rules' => 'required|numeric'
					],
					[
						'field' => 'category_name',
						'label' => 'Sub Category',
						'rules' => 'required|alpha_numeric_spaces'
					]
				];
				if(Validate::form($rule)){
					$this->db->insert("categories", $sql_data);
					Notify::setSuccess("Dodata nova podkategorija ".$sql_data['category_name'].".");
					redirect($this->controller_url."categories");
				} else Notify::setError("Pogresan format podataka!");			
			}
		}
		# Delete subcategory
		if ($this->input->post("delete_subcategory")) {
			$ID = $this->input->post("delete_subcategory");
			$category_name = $this->input->post("category_name");
			if (Security::csrfCheck($this->input->post('csrf_token'))) {
				$this->m_admin->deleteRow('categories', $ID);
				Notify::setSuccess("Izbrisano! ".$category_name);
				redirect($this->controller_url."categories");
			}
		}
		//printr($data['query_kategorije']);

		$data['class'] = __CLASS__;
		$data['page'] = __FUNCTION__;
		$data['action'] = 'Upis podataka';
		$data['pageName'] = 'categories'; 
		$data['msg'] = Notify::getMessage();
		$data['csrf_token'] = Security::csrf();
		#$data['lng'] = LNG;
		#$data['lngName'] = $this->m_languages->getLangName(LNG);
		$this->session->set_userdata(['csrf_token' => $data['csrf_token']]);

		# Zbog csrf zastite
		$this->load->view('admin/product_settings/categories', $data);
	}


	public function Sizes($value='')
	{
		$where = 'parent_id = 0 ';
		$data['sql_sizes'] = $this->m_admin->select('id, size_name', 'sizes');
		$data['sql_categories'] = $this->m_admin->select('id, category_name', 'categories', $where);

		# Add new size
		#$this->addSize();
		if ($this->input->post("save_size")) {
			if (Security::csrfCheck($this->input->post('csrf_token'))) {
				$rule = [
					[
						'field' => 'category_id',
						'label' => 'Category',
						'rules' => 'required|numeric'
					],
					[
						'field' => 'sub_category',
						'label' => 'Sub Category',
						'rules' => 'required|numeric'
					],
					[
						'field' => 'size[]',
						'label' => 'Sizes',
						'rules' => 'char_plus'
					]
					
				];
				if(Validate::form($rule)){
					$sql_data = [
						"category_id" => $this->input->post("category_id"),
						"sub_category" => $this->input->post("sub_category")
					];
					$sizes = "";
					foreach ($this->input->post("size") as $value) {
						if ($value != "") {
							$sizes .= $value.", ";
							$sql_data['size_name'] = $value;
							$this->db->insert("sizes", $sql_data);
						}					
					}
					$sizes = rtrim($sizes,", ");
					Notify::setSuccess("Dodate nove velicine: ".$sizes.".");
					redirect($this->controller_url."sizes");
				} else Notify::setError("Pogresan format podataka!");
			} 
		}

		# Delete size
		if ($this->input->post("delete_size")) {
			if (Security::csrfCheck($this->input->post('csrf_token'))) {
				$ID = $this->input->post("delete_size");
				$size_name = $this->input->post("size_name");
				$this->m_admin->deleteRow('sizes', $ID);
				Notify::setSuccess("Izbrisana velicia: ".$size_name.".");
				redirect($this->controller_url."sizes");
			}
		}
		//printr($data['query_kategorije']);

		$data['class'] = __CLASS__;
		$data['page'] = __FUNCTION__;
		$data['action'] = 'Upis podataka';
		$data['pageName'] = 'sizes'; 
		$data['csrf_token'] = Security::csrf();
		$data['msg'] = Notify::getMessage();
		#$data['lng'] = LNG;
		#$data['lngName'] = $this->m_languages->getLangName(LNG);

		return $this->load->view('admin/product_settings/sizes.php', $data);
	}

	public function Colors($value='')
	{
		$this->load->library('unit_test');
		$this->unit->use_strict(TRUE); //onda vazi 1 != TRUE
		$this->unit->active(FALSE);

		$data['sql_colors'] = $this->m_admin->select('id, color_name', 'colors');

		$result = 1;
		$this->unit->run($result, "is_int", "Test case");
		echo $this->unit->report();

		
		$data['class'] = __CLASS__;
		$data['page'] = __FUNCTION__;
		$data['pageName'] = 'colors'; 
		$data['msg'] = Notify::getMessage();
		$data['csrf_token'] = Security::csrf();
		$data['include_css'] = "flex.css";
		$data['this_url'] = base_url().$this->controller_url;
		return $this->load->view('admin/product_settings/colors.php', $data);
	}

	/**
	 * Saves new item to database
	 * @return [type]        [description]
	 */
	public function store_color($value='')
	{
		
			$sql_data = [
				'color_name' => $this->input->post("color_name")
			];
			$rule = [
				[
					'field' => 'color_name',
					'label' => 'Color name',
					'rules' => 'required|alpha_numeric_spaces'
				]
			];
			if(Validate::form($rule)){
				$this->db->insert("colors", $sql_data);
				Notify::setSuccess("Nova boja sacuvana ".$sql_data['color_name'].".");
				redirect($this->controller_url);
			} else Notify::setError("Pogresan format podataka!");	
			redirect($this->controller_url);		
		

	}

	#New
	public function update_color($value='')
	{
		printr($this->uri);
	}

	public function show(Request $request)
	{
		printr($request->color_name);
		printr($request->total());
		printr($request->to_array());
	}

	/**
	 * Delete item from database
	 * @return [type] [description]
	 */
	public function delete_color(Request $r)
	{
			$ID = (int)$this->input->post("id");

			if(Validate::check('int',$ID)){
				#$this->m_admin->deleteRow('colors', $ID);
				Notify::setSuccess("Izbrisana boja.");
				redirect($this->controller_url);
			} else Notify::setError("Pogresan format podataka!");	
			redirect($this->controller_url);		

	}

	public function test($value='')
	{
		printr($this->input->server('HTTP_REFERER'));
		$reffere = $this->input->server('HTTP_REFERER');
		if ($this->input->post('_method')) {
			echo "_method: ".$this->input->post('_method');
		} else redirect($reffere);
		
		return;
	}

}

 ?>