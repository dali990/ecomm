<?php 
declare(strict_types=1);
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Security;
use App\Validate;
use App\Notify;

class Sizes extends CI_Controller {
	protected $controller_url;
	public function __construct($id='')
	{ 
		parent::__construct();
		$this->controller_url = "admin/properties/".strtolower(__CLASS__);
		$this->load->library('session');
		$this->load->model('m_home');
		$this->load->model('config/m_admin');
		$this->load->helper(array('form', 'url', 'dataviz', 'isempty'));

		$this->session->set_userdata('ajax_request',"true");

	}

	public function index()
	{	

		$this->Sizes();

	}

	
	public function Sizes($value='')
	{
		$where = 'parent_id = 0 ';
		$data['sql_sizes'] = $this->m_admin->select('id, size_name', 'sizes');
		$data['sql_categories'] = $this->m_admin->select('id, category_name', 'categories', $where);

		

		$data['class'] = __CLASS__;
		$data['page'] = __FUNCTION__;
		$data['action'] = 'Upis podataka';
		$data['pageName'] = 'sizes'; 
		$data['csrf_token'] = Security::csrf();
		$data['msg'] = Notify::getMessage();
		#$data['lng'] = LNG;
		#$data['lngName'] = $this->m_languages->getLangName(LNG);

		return $this->load->view('admin/product_settings/sizes.php', $data);
	}

	/**
	 * Saves new item to database
	 * @return [type]        [description]
	 */
	public function store(Request $r)
	{
		
		$rule = [
			[
				'field' => 'category_id',
				'label' => 'Category',
				'rules' => 'required|numeric'
			],
			[
				'field' => 'sub_category',
				'label' => 'Sub Category',
				'rules' => 'required|numeric'
			],
			[
				'field' => 'size[]',
				'label' => 'Sizes',
				'rules' => 'char_plus'
			]
			
		];
		if(Validate::form($rule)){
			$sql_data = [
				"category_id" => $r->category_id,
				"sub_category" => $r->sub_category
			];
			$sizes = "";
			foreach ($this->input->post("size") as $value) {
				if ($value != "") {
					$sizes .= $value.", ";
					$sql_data['size_name'] = $value;
					$this->db->insert("sizes", $sql_data);
				}					
			}
			$sizes = rtrim($sizes,", ");
			Notify::setSuccess("Dodate nove velicine: ".$sizes.".");
			redirect($this->controller_url);
		} else Notify::setError("Pogresan format podataka!");
		redirect($this->controller_url);		
		

	}

	#New
	public function update($value='')
	{
		printr($this->uri);
	}

	public function show(Request $request)
	{
		
	}

	/**
	 * Delete item from database
	 * @return [type] [description]
	 */
	public function delete(Request $r)
	{
		$ID = (int)$r->size_id;

		if(Validate::check('int',$ID)){
			$this->m_admin->deleteRow('sizes', $ID);
			Notify::setSuccess("Izbrisana velicina.");
			redirect($this->controller_url);
		} else Notify::setError("Pogresan format podataka!");	
		redirect($this->controller_url);		

	}

}

 ?>