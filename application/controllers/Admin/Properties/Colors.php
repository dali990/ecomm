<?php declare(strict_types=1);
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Security;
use App\Validate;
use App\Notify;
/**
* 	
*/
class Colors extends MY_Controller
{
	protected $controller_url;
	public function __construct($id='')
	{ 
		parent::__construct();
		$this->controller_url = "admin/properties/".strtolower(__CLASS__);
		$this->load->library('session');
		$this->load->model('config/m_admin');
		$this->load->helper(array('dataviz','form'));

		$this->session->set_userdata('ajax_request',"true");

	}

	public function index($value='')
	{
		$this->load->library('unit_test');
		$this->unit->use_strict(TRUE); //onda vazi 1 != TRUE
		$this->unit->active(FALSE);

		$data['sql_colors'] = $this->m_admin->select('id, color_name', 'colors');

		$result = 1;
		$this->unit->run($result, "is_int", "Test case");
		echo $this->unit->report();

		
		$data['class'] = __CLASS__;
		$data['page'] = __FUNCTION__;
		$data['pageName'] = 'colors'; 
		$data['msg'] = Notify::getMessage();
		$data['csrf_token'] = Security::csrf();
		$data['include_css'] = "flex.css";
		$data['this_url'] = base_url().$this->controller_url;
		return $this->load->view('admin/product_settings/colors.php', $data);
	}

	/**
	 * Saves new item to database
	 * @return [type]        [description]
	 */
	public function store(Request $r)
	{
		
			$sql_data = [
				'color_name' => $this->input->post("color_name")
			];
			$rule = [
				[
					'field' => 'color_name',
					'label' => 'Color name',
					'rules' => 'required|alpha_numeric_spaces'
				]
			];
			if(Validate::form($rule)){
				$this->db->insert("colors", $sql_data);
				Notify::setSuccess("Nova boja sacuvana ".$sql_data['color_name'].".");
				redirect($this->controller_url);
			} else Notify::setError("Pogresan format podataka!");	
			redirect($this->controller_url);		
		

	}

	#New
	public function update($value='')
	{
		printr($this->uri);
	}

	public function show(Request $request)
	{
		printr($request->color_name);
		printr($request->total());
		printr($request->to_array());
	}

	/**
	 * Delete item from database
	 * @return [type] [description]
	 */
	public function delete(Request $r)
	{
		$ID = (int)$r->color_id;

		if(Validate::check('int',$ID)){
			$this->m_admin->deleteRow('colors', $ID);
			Notify::setSuccess("Izbrisana boja.");
			redirect($this->controller_url);
		} else Notify::setError("Pogresan format podataka!");	
		redirect($this->controller_url);		

	}

	public function delete_sub(Request $r)
	{
		printr($r);		

	}
}

/* End of file Colors.php */
/* Location: ./application/controllers/Admin/Colors.php */