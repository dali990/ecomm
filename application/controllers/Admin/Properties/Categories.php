<?php 
declare(strict_types=1);
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Security;
use App\Validate;
use App\Notify;

class Categories extends MY_Controller {
	protected $controller_url;
	public function __construct($id='')
	{ 
		parent::__construct();
		$this->controller_url = "admin/properties/".strtolower(__CLASS__);
		$this->load->library('session');
		$this->load->model('m_home');
		#$this->load->model('m_notify');
		$this->load->model('config/m_admin');
		$this->load->helper(array('form', 'url', 'dataviz', 'isempty'));
		#$this->load->model('config/m_errorHandler');
		#Security::csrfReset();
		/*$this->load->helper('file');
		$now = DateTime::createFromFormat('U.u', microtime(true));
		write_file('csrf.txt', $now->format("m-d-Y H:i:s.u")."\n", 'a');*/

		/*$data = $this->security->xss_clean("<h2>TEST</h2><script>alert('hack')</script>");
		$sess =  $this->security->get_csrf_hash();
		echo $sess;*/
		//Ajax security
		$this->session->set_userdata('ajax_request',"true");

	}

	public function index()
	{	

		$this->Categories();
		/*echo "Product";
		Notify::getBootstrap();
		echo Notify::getMessage();
		printr(Notify::getData());*/
	}

	public function Categories($value='')
	{	
		$where = 'parent_id = 0 ';
		$data['sql_categories'] = $this->m_admin->select('id, category_name', 'categories', $where);

		

		$data['class'] = __CLASS__;
		$data['page'] = __FUNCTION__;
		$data['action'] = 'Upis podataka';
		$data['pageName'] = 'categories'; 
		$data['msg'] = Notify::getMessage();
		$data['csrf_token'] = Security::csrf();
		#$data['lng'] = LNG;
		#$data['lngName'] = $this->m_languages->getLangName(LNG);
		$this->session->set_userdata(['csrf_token' => $data['csrf_token']]);

		# Zbog csrf zastite
		$this->load->view('admin/product_settings/categories', $data);
	}

	/**
	 * Saves new item to database
	 * @return [type]        [description]
	 */
	public function store(Request $r)
	{
		$sql_data = [
			'category_name' => $r->category_name,
			'parent_id' => 0
		];
		$rule = [
			[
				'field' => 'category_name',
				'label' => 'Category',
				'rules' => 'required|alpha_numeric_spaces'
			]
		];
		if(Validate::form($rule)){
			$this->db->insert("categories", $sql_data);
			Notify::setSuccess("Nova kategorija uspesno sacuvana!");
			redirect($this->controller_url);
		} else Notify::setError("Pogresan format podataka!");	
		redirect($this->controller_url);		
		
	}

	public function store_subcategory(Request $r)
	{
		$sql_data = [
			'parent_id' => $r->parent_id,
			'category_name' => $r->category_name
		];
		$rule = [
			[
				'field' => 'parent_id',
				'label' => 'Category',
				'rules' => 'required|numeric'
			],
			[
				'field' => 'category_name',
				'label' => 'Sub Category',
				'rules' => 'required|alpha_numeric_spaces'
			]
		];
		if(Validate::form($rule)){
			$this->db->insert("categories", $sql_data);
			Notify::setSuccess("Dodata nova podkategorija ".$sql_data['category_name'].".");
			redirect($this->controller_url);
		} else Notify::setError("Pogresan format podataka!");
		redirect($this->controller_url);		
	}

	#New
	public function update($value='')
	{
		printr($this->uri);
	}

	public function show(Request $request)
	{
		printr($request->color_name);
		printr($request->total());
		printr($request->to_array());
	}

	/**
	 * Delete item from database
	 * @return [type] [description]
	 */
	public function delete(Request $r)
	{
		$ID = (int)$r->category_id;

		if(Validate::check('int',$ID)){
			$this->m_admin->deleteRow('categories', $ID);
			Notify::setSuccess("Izbrisana kategorija.");
			redirect($this->controller_url);
		} else Notify::setError("Pogresan format podataka!");	
		redirect($this->controller_url);		

	}


}

 ?>