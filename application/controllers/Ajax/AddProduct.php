<?php declare(strict_types=1);
defined('BASEPATH') OR exit('No direct script access allowed');
use App\Validate;
use App\RestGuard;

class AddProduct extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		// Auth check for admin!

		RestGuard::REST();
	}

	public function getSizes(int $id=null, int $sub_id)
	{

		$result = $this->categorySizes($id,$sub_id);
		echo json_encode($result);

	}

	public function categorySizes(int $id, int $sub_id)
	{	
		$this->db->select('size_name, id');
		$this->db->where('category_id', $id);
		$this->db->where('sub_category', $sub_id);
		return $this->db->get('sizes')->result_array();
	}

	public function getSubCategories(int $id)
	{
		$this->db->select('category_name, id');
		$this->db->where('parent_id', $id);
		$result = $this->db->get('categories')->result_array();
		echo json_encode($result);
	}

	public function getColors()
	{	
		$this->db->select('color_name, id');
		echo json_encode($this->db->get('colors')->result_array());
		return;
	}

}

 ?>