<?php
defined('BASEPATH') OR exit('No direct script access allowed');
#use Lib\DB;
use App\TestModule;
use App\Validate;
use App\Notify;

class Home extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		/*$this->load->library('MYDB');
		$this->mydb->init();*/
	}

	public function index()
	{	
		echo Validate::check('address','Mikanović 25   ');
		#DB::init("TEST");
		TestModule::init();
		echo "<hr>";
		//$this->session->set_flashdata('error_flash', "TEST ERROR");
		echo Notify::getError();

		$this->load->library('form_validation');
		$data['msg'] = '1';
		$data['error_fields'] = $this->form_validation->error_array();
		echo uniqid();
		$this->load->view('home', $data);
	}

	public function get($value='')
	{	
		$rule = [
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email'
			],
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|max_length[12]|alpha_numeric'
			]
			
		];
		$Validate = Validate::form($rule);
		$data['error_fields'] = $this->form_validation->error_array();
		if(!$Validate) $this->load->view('home', $data);
		$arr = [
			'name'=> 'Dalibor',
			'age' => 27,
			'address'=> 'Fake Main Street 88'
		];
		$validate = Validate::arrayCheck($arr,'int');
		if($validate){
			echo "<h3>Array OK</h3>";
		} else {echo "<h3>Array FALSE</h3>"; }
		# Debuging if faild
		printr(Validate::showArrayStatus());
		
		/* $this->load->helper('form');
		$this->load->library('form_validation');
		//echo $this->session->item;
		$rule = [
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email'
			],
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required|max_length[12]|alpha_numeric'
			]
			
		];
		$this->form_validation->set_rules($rule);

		if ($this->form_validation->run() == FALSE)
    {
    	#$this->index();
      #$this->load->view('myform');
      #return false;
      $this->load->view('home');
      //redirect(base_url().'home');
    }
    else
    {	
    	//$this->load->view('home');
      #return true;
      echo "Validation passed!";
    }

		echo "$value<br>";
		#echo $this->input->post("email", TRUE);
		*/
	} 

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */